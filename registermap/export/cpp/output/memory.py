#
# Copyright 2018 Russell Smiley
#
# This file is part of registermap.
#
# registermap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registermap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

from ....export.commonCppC.output import MemoryTemplatesBase
from .base import CPP_TEMPLATE_PACKAGE

MEMORY_TEMPLATE_CONFIGURATION = {
    "header": [
        {
            "file": "memory.hpp",
            "template": "memory_template.hpp",
        },
    ],
    "source": [
        {
            "file": "memory.cpp",
            "template": "memory_template.cpp",
        },
    ],
    "template-package": CPP_TEMPLATE_PACKAGE,
}


class MemoryTemplates(MemoryTemplatesBase):
    def __init__(
        self,
        paths,
        registermapName,
        encapsulatedMemory,
        licenseTextLines=None,
        suffixes=list(),
    ):
        super().__init__(
            paths,
            MEMORY_TEMPLATE_CONFIGURATION,
            registermapName,
            encapsulatedMemory,
            licenseTextLines=licenseTextLines,
            suffixes=suffixes,
        )
