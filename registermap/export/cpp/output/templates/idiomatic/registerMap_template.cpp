/*
 *
 * {{ registermap.name }}
 *
 {%- if licenseText is not none %}
 {%- for line in licenseText %}
 * {{ line }}
 {%- endfor %}
 {%- endif %}
 *
 */

#include "{{ prefixPath }}/registermap.hpp"


namespace {{ registermap.name }}
{

  {{ registermap.name }}_t {{ registermap.name }};

}
