/*
 *
 * {{ registermap.name }}
 *
 {%- if licenseText is not none %}
 {%- for line in licenseText %}
 * {{ line }}
 {%- endfor %}
 {%- endif %}
 *
 */

#ifndef {{ registermap.name|upper }}_HPP
#define {{ registermap.name|upper }}_HPP

{% for thisModule in registermap.modules -%}
#include "{{ prefixPath }}/modules/{{ thisModule.name }}.hpp"
{% endfor %}

{% set registermapType = registermap.name~'_t' -%}
namespace {{ registermap.name }}
{
  {%- if registermap.modules|count != 0 %}

#pragma pack( {{ registermap.memory.alignment }} )

  class {{ registermapType }}
  {
    public:
      MemorySpace memory;

      {{ registermapType }}() :
      {%- for thisModule in registermap.modules %}
      {%- set pointerType = thisModule.name~'_t volatile* const' %}
          {{ thisModule.name }}( reinterpret_cast<{{ pointerType }}>( memory.base + {{ thisModule.offset }} ) ){{ "," if not loop.last }}
      {%- endfor %}
        {};
{% for thisModule in registermap.modules %}
      {%- set pointerType = thisModule.name~'_t volatile* const' %}
      {{ pointerType }} {{ thisModule.name }};
{%- endfor %}
  };

#pragma pack()


  // Declare the register map instance for users.
  extern {{ registermap.name }}_t {{ registermap.name }};
  {%- endif %}

}

#endif
