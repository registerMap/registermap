#
# Copyright 2018 Russell Smiley
#
# This file is part of registermap.
#
# registermap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registermap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

import logging
import os

from ..base import OutputBase
from .elements import Memory  # noqa: F401
from .elements import RegisterMap
from .output import (
    MacroTemplates,
    MemoryTemplates,
    ModuleTemplates,
    RegisterMapTemplates,
)

log = logging.getLogger(__name__)


class Output(OutputBase):
    """
    Export a C source code representation of the register map.
    """

    def __init__(self, outputOptions, licenseTextLines=list()):
        super().__init__(outputOptions, licenseTextLines=licenseTextLines)

        self.includePath = None
        self.sourceDirectory = None

        self.suffixes = [".h", ".c"]

    def __createDirectories(self, registermapName):
        self.includePath = os.path.join(
            self.outputDirectory,
            "include",
            self.outputOptions.includePrefix,
            registermapName,
        )
        self.createDirectory(self.includePath)

        self.sourceDirectory = os.path.join(
            self.outputDirectory, "source", registermapName
        )
        self.createDirectory(self.sourceDirectory)

    def generate(self, registermap, registermapName):
        """
        Export the register map representation.

        :param registermap: A RegisterMap instance for export.
        :param registermapName: A public name for the register map used in source code file structure and object names.
        """
        self.__createDirectories(registermapName)

        encapsulatedRegisterMap = RegisterMap(registermapName, registermap)
        encapsulatedRegisterMap.memory.alignment = (
            self.outputOptions.packAlignment
        )

        paths = MacroTemplates.Paths()
        paths.includeDirectory = self.includePath
        paths.includePrefix = os.path.join(
            self.outputOptions.includePrefix, registermapName
        )
        paths.sourceDirectory = self.sourceDirectory

        buildOrder = [
            MacroTemplates(
                paths,
                registermapName,
                licenseTextLines=self.licenseTextLines,
                suffixes=self.suffixes,
            ),
            MemoryTemplates(
                paths,
                registermapName,
                encapsulatedRegisterMap.memory,
                licenseTextLines=self.licenseTextLines,
                suffixes=self.suffixes,
            ),
            ModuleTemplates(
                paths,
                encapsulatedRegisterMap,
                licenseTextLines=self.licenseTextLines,
                suffixes=self.suffixes,
            ),
            RegisterMapTemplates(
                paths,
                encapsulatedRegisterMap,
                licenseTextLines=self.licenseTextLines,
                suffixes=self.suffixes,
            ),
        ]

        for build in buildOrder:
            build.apply()
