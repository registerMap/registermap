/*
 *
 * Module: {{ module.name }}
 *
 {%- if licenseText is not none %}
 {%- for line in licenseText %}
 * {{ line }}
 {%- endfor %}
 {%- endif %}
 *
 */

#ifndef {{ registermapName|upper }}_{{ module.name|upper }}_H
#define {{ registermapName|upper }}_{{ module.name|upper }}_H

#include <stdint.h>

#include "{{ prefixPath }}/macro/extern.h"
#include "{{ prefixPath }}/memory/memory.h"
#include "{{ prefixPath }}/registermap.h"

{% if module.registers|count != 0 %}
{{ registermapName|upper }}_OPEN_EXTERN_C
{%- for thisRegister in module.registers %}
{%- set canonicalRegisterName = module.name~'_'~thisRegister.name %}
{%- set typeName = canonicalRegisterName~'_t' %}

struct {{ registermapName }}_{{ typeName }}
{
{%- for bitField in thisRegister.fields %}
  {{ bitField.type }} volatile {{ bitField.name }}:{{ bitField.size }};
{%- endfor %}
};
{% endfor %}

#pragma pack( {{ registermapMemoryAlignment }} )

struct {{ registermapName }}_{{ module.name }}_t
{
{%- for thisRegister in module.registers %}
{%- if thisRegister.precedingGapBytes != 0 %}
  uint8_t gap_{{ thisRegister.name }}[{{ thisRegister.precedingGapBytes }}];
{%- endif %}
{%- set canonicalRegisterName = module.name~'_'~thisRegister.name %}
{%- set typeName = canonicalRegisterName~'_t volatile' %}
  struct {{ registermapName }}_{{ typeName }} {{ thisRegister.name }};
{% endfor -%}
};

#pragma pack()


{{ registermapName|upper }}_CLOSE_EXTERN_C
{%- else %}
// {{ registermapName }}_{{ module.name }} is an empty module
{%- endif %}

#endif
