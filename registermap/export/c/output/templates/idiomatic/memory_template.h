/*
 *
 * {{ registermapName }}
 *
 {%- if licenseText is not none %}
 {%- for line in licenseText %}
 * {{ line }}
 {%- endfor %}
 {%- endif %}
 *
 */

#ifndef {{ registermapName|upper }}_MEMORY_H
#define {{ registermapName|upper }}_MEMORY_H

#include <stdint.h>

#include "{{ prefixPath }}/macro/extern.h"


{{ registermapName|upper }}_OPEN_EXTERN_C

{% set memoryPointerType = memory.sizeType~' volatile* const' -%}
struct {{ registermapName }}_MemorySpace_t
{
#ifdef OFF_TARGET_MEMORY

  uint_least32_t const allocated_memory_span;

  {{ memory.sizeType }} volatile base[ {{ memory.size }} ];

#else

  {{ memoryPointerType }} base;

#endif
};

{{ registermapName|upper }}_CLOSE_EXTERN_C

#endif
