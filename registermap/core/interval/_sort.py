#
# Copyright 2018 Russell Smiley
#
# This file is part of registermap.
#
# registermap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registermap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

from ._element import ClosedIntegerInterval, IntervalList
from ._overlap import any_overlap


def _interval_sort_key(interval):
    return min(interval.value)


def sort_intervals(intervals: IntervalList) -> IntervalList:
    """Sort a list or set of integer intervals `ClosedIntegerInterval`

    Sorted into a list of increasing order.

    eg.

      { (3, 4), (0, 2), (5, 7) } becomes [ (0, 2), (3, 4), (5, 7) ]

    Args:
        intervals: List of intervals to be sorted.

    Returns:
        List of sorted intervals.
    """
    assert not any_overlap(intervals)

    sorted_intervals = sorted(list(intervals), key=_interval_sort_key)

    return sorted_intervals
