#
# Copyright 2018 Russell Smiley
#
# This file is part of registermap.
#
# registermap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registermap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

import logging
import typing

from .exceptions import IntervalError

log = logging.getLogger(__name__)

T = typing.TypeVar("T", bound="ClosedIntegerInterval")

UIT = typing.Optional[typing.Union[T, int, set, tuple, list]]


class ClosedIntegerInterval:
    """An integer mathematically closed interval.

    The internal representation is `frozenset`, but `set`, `list` and `tuple`
    can be used to initialise the interval, and for comparison.
    """

    __valid_containers = (set, tuple, list)

    def __init__(self: T, value: UIT = None) -> None:
        if value is not None:
            self.__validate_value(value)
            self.__value = frozenset(value)
        else:
            self.__value = value

    @property
    def size(self: T) -> int:
        if self.__value is None:
            this_size = 0
        else:
            this_size = max(self.__value) - min(self.value) + 1

        return this_size

    @property
    def value(self: T):
        return self.__value

    @value.setter
    def value(self: T, value):
        self.__validate_value(set(value))
        self.__value = frozenset(value)

    def __validate_value(self: T, value: UIT) -> None:
        error_message = (
            f"Interval must be a set of one or two positive integers, {value}"
        )
        if not isinstance(value, self.__valid_containers):
            raise IntervalError(error_message)

        if any([not isinstance(x, int) for x in value]):
            raise IntervalError(error_message)

        if any([x < 0 for x in value]):
            raise IntervalError(error_message)

        if len(value) not in [1, 2]:
            raise IntervalError(error_message)

    def __eq__(self: T, other: UIT) -> bool:
        if other is None:
            is_equal = self.__value is other
        elif isinstance(other, self.__valid_containers):
            is_equal = self.__value == frozenset(other)
        elif other.__value is None:
            is_equal = self.__value is other.__value
        else:
            is_equal = self.__value == other.__value

        return is_equal

    def __ne__(self: T, other: T):
        return not (self == other)

    def __hash__(self: T):
        # Python seems to think the ClosedIntegerInterval class is not hashable
        # (when used as an index in a dict), so explicitly defining __hash__
        # method and returning the object id fixes that.
        return hash(self.__value)

    def __add__(self: T, other: int) -> T:
        if isinstance(other, int):
            result = ClosedIntegerInterval(
                value=[x + other for x in self.__value]
            )
        else:
            raise IntervalError("Must add int to ClosedIntegerInterval")

        return result

    def __sub__(self: T, other: int) -> T:
        if isinstance(other, int):
            result = ClosedIntegerInterval(
                value=[x - other for x in self.__value]
            )
        else:
            raise IntervalError("Must add int to ClosedIntegerInterval")

        return result


IntervalList = typing.List[ClosedIntegerInterval]
