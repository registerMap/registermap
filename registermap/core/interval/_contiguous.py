#
# Copyright 2018 Russell Smiley
#
# This file is part of registermap.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

"""Contiguity of intervals."""

from ._element import ClosedIntegerInterval, IntervalList
from ._sort import sort_intervals


def make_contiguous(intervals: IntervalList, max_index: int) -> IntervalList:
    """Make the list or set of intervals contiguous.

    Inserts intervals in any necessary gaps.

    Args:
        intervals:  List of intervals to be made contiguous.
        max_index:  The maximum index in the intervals. Enables inserting an
                    interval at the end if necessary.

    Returns:
        List of revised intervals
    """
    sorted_intervals = sort_intervals(intervals)

    contiguous_intervals = list()

    # Check for gap at the start
    if min(sorted_intervals[0].value) != 0:
        max_this_interval = min(sorted_intervals[0].value) - 1
        contiguous_intervals.append(
            ClosedIntegerInterval((0, max_this_interval))
        )

    for index in range(0, (len(sorted_intervals) - 1)):
        contiguous_intervals.append(sorted_intervals[index])

        min_next_interval = min(sorted_intervals[index + 1].value)
        expected_min_next_interval = max(sorted_intervals[index].value) + 1
        if min_next_interval != expected_min_next_interval:
            contiguous_intervals.append(
                ClosedIntegerInterval(
                    (expected_min_next_interval, (min_next_interval - 1))
                )
            )

    # Always append the last interval
    contiguous_intervals.append(sorted_intervals[-1])

    max_last_interval = max(sorted_intervals[-1].value)
    if max_last_interval != max_index:
        # Add an interval at the end to fill the gap to maxIndex.
        contiguous_intervals.append(
            ClosedIntegerInterval(((max_last_interval + 1), max_index))
        )

    return contiguous_intervals
