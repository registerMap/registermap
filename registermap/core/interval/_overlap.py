#
# Copyright 2017 Russell Smiley
#
# This file is part of registermap.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

"""Interval comparisons."""

from ._element import ClosedIntegerInterval, IntervalList


def is_overlap(
    interval1: ClosedIntegerInterval, interval2: ClosedIntegerInterval
) -> bool:
    """Examine two intervals for any overlap.

    Args:
        interval1: First interval to be compared.
        interval2: Second interval to be compared.

    Returns:
        True if the intervals overlap in any way.
    """
    if (max(interval1.value) >= min(interval2.value)) and (
        max(interval1.value) <= max(interval2.value)
    ):
        return True
    elif (min(interval1.value) <= max(interval2.value)) and (
        min(interval1.value) >= min(interval2.value)
    ):
        return True
    elif (min(interval2.value) <= max(interval1.value)) and (
        min(interval2.value) >= min(interval1.value)
    ):
        return True

    return False


def is_encapsulated(
    interval1: ClosedIntegerInterval, interval2: ClosedIntegerInterval
) -> bool:
    """Examine the first interval for encapsulation by the second interval.

    Args:
        interval1: First interval to be compared.
        interval2: Second interval to be compared.

    Returns:
        True if interval1 is encapsulated by interval2 (including that they are equal).
    """
    if (max(interval1.value) <= max(interval2.value)) and (
        min(interval1.value) >= min(interval2.value)
    ):
        return True

    return False


def any_overlap(intervals: IntervalList):
    """Detect any overlapping intervals in an iterable of intervals.

    Args:
        intervals: List of intervals to be tested.

    Returns:
        True if any intervals in the iterable overlap.
    """
    reviewed_overlap = set()

    interval_set = set(intervals)

    while interval_set:
        reviewing_interval = interval_set.pop()
        reviewed_overlap.add(
            any({is_overlap(reviewing_interval, x) for x in interval_set})
        )

    return any(reviewed_overlap)
