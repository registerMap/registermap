#  Copyright (c) 2023 Russell Smiley
#
#  This file is part of registermap.
#
#  You should have received a copy of the GNU General Public License
#  along with registermap.  If not, see <http://www.gnu.org/licenses/>.

import re
import typing

import pydantic
import pydantic_core

NAME_PATTERN = r"[A-Za-z][0-9a-zA-Z\-_]*[0-9]+"
STRUCTURED_NAME_PATTERN = r"(?P<module_global_field>{0})(\.(?P<register>{0})(\.(?P<field>{0}))?)?".format(
    NAME_PATTERN
)

T = typing.TypeVar("T", bound="StructuredName")


class StructuredName:
    """Structured name of a register map element.

    Indicates parent elements, if any.
    """

    module_global_field: str
    register: typing.Optional[str]
    field: typing.Optional[str]

    def __init__(self: T, value: str) -> None:
        r = re.match(STRUCTURED_NAME_PATTERN, value)
        if r is None:
            raise RuntimeError(
                f"Invalid structured register map element name, {value}"
            )
        else:
            self.module_global_field = r.group("module_global_field")
            self.register = r.group("register")
            self.field = r.group("field")

    def __str__(self: T) -> str:
        return self.__generate_structured_string()

    def __generate_structured_string(self: T):
        """Generate string representation of the structured name."""
        if self.register is None:
            v = self.module_global_field
        elif self.field is None:
            v = f"{self.module_global_field}.{self.register}"
        else:
            v = f"{self.module_global_field}.{self.register}.{self.field}"

        return v

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: typing.Any, handler: pydantic.GetCoreSchemaHandler
    ) -> pydantic_core.CoreSchema:
        return pydantic_core.core_schema.no_info_after_validator_function(
            cls, handler(str)
        )


OptionalStructuredName = typing.Optional[StructuredName]
