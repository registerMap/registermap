#
# Copyright 2017 Russell Smiley
#
# This file is part of registermap.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

"""Core objects consumed by other parts of ``registermap``."""

from ._memory_annotations import *  # noqa: F401
from .parameters import ElementsParameter, ModulesParameter  # noqa: F401
