#
# Copyright 2017 Russell Smiley
#
# This file is part of registermap.
#
# registermap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registermap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

"""Define core register map element properties used by other elements."""

from .bitStore import BitStoreInterface  # noqa: F401
from .element import ElementList  # noqa: F401
from .identity import IdentityElement  # noqa: F401
from .observable import (  # noqa: F401
    AddressObservableInterface,
    SizeObservableInterface,
)
