#  Copyright (c) 2017 Russell Smiley
#
#  This file is part of registermap.
#
#  You should have received a copy of the GNU General Public License
#  along with registermap.  If not, see <http://www.gnu.org/licenses/>.

import pytest

from registermap.registerMap import RegisterMap


@pytest.fixture()
def map_under_test() -> RegisterMap:
    m = RegisterMap()

    assert len(m) == 0

    return m


class TestRegisterMapLen:
    def test_module_len1(self, map_under_test):
        map_under_test.addModule("myModule")

        assert len(map_under_test) == 1

    def test_register_len2(self, map_under_test):
        my_module = map_under_test.addModule("myModule")

        my_module.addRegister("myRegister")

        assert len(map_under_test) == 2

    def test_field_len3(self, map_under_test):
        my_module = map_under_test.addModule("myModule")

        my_register = my_module.addRegister("myRegister")

        my_register.addField("myField", [0, 3])

        assert len(map_under_test) == 3

    def test_second_local_field_len4(self, map_under_test):
        my_module = map_under_test.addModule("myModule")

        my_register = my_module.addRegister("myRegister")

        my_register.addField("myField", [0, 3])
        my_register.addField("myOtherField", [0, 3])

        assert len(map_under_test) == 4

    def test_global_field_len3(self, map_under_test):
        my_module = map_under_test.addModule("myModule")

        my_register = my_module.addRegister("myRegister")

        my_register.addField("myField", [0, 3], (0, 3), isGlobal=True)
        my_register.addField("myField", [4, 6], (4, 6), isGlobal=True)

        assert len(map_under_test) == 3
