#
# Copyright 2018 Russell Smiley
#
# This file is part of register_map.
#
# You should have received a copy of the GNU General Public License
# along with register_map.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.exceptions import ConstraintError
from registermap.registerMap import Module, RegisterMap


class TestRegisterFixedAddressConstraint:
    class ElementsUnderTest:
        def __init__(self):
            self.register_map = RegisterMap()
            self.module = self.register_map.addModule("m1")
            self.r1 = self.module.addRegister("r1")

    @pytest.fixture()
    def elements_under_test(self) -> ElementsUnderTest:
        return TestRegisterFixedAddressConstraint.ElementsUnderTest()

    def test_fixed_address_ok(self, elements_under_test):
        """Applying a fixed address constraint changes the address."""
        assert 0x0 == elements_under_test.register_map.memory.baseAddress
        assert (
            elements_under_test.register_map.memory.baseAddress
            == elements_under_test.module.baseAddress
        )
        assert (
            elements_under_test.module.baseAddress
            == elements_under_test.r1.startAddress
        )

        expected_value = 0x2000

        elements_under_test.r1["constraints"]["fixedAddress"] = expected_value

        assert expected_value == elements_under_test.r1.startAddress

    def test_fixed_address_adjusts_other_registers(self, elements_under_test):
        """Applying a fixed address shuffles other registers appropriately."""

        r2 = elements_under_test.module.addRegister("r2")

        assert 0x0 == elements_under_test.register_map.memory.baseAddress
        assert (
            elements_under_test.register_map.memory.baseAddress
            == elements_under_test.module.baseAddress
        )
        assert (
            elements_under_test.module.baseAddress
            == elements_under_test.r1.startAddress
        )

        assert 1 == elements_under_test.r1.sizeMemoryUnits
        assert 1 == r2.startAddress

        expected_value = 0x2000

        elements_under_test.r1["constraints"]["fixedAddress"] = expected_value

        assert expected_value == elements_under_test.r1.startAddress
        assert (expected_value + 1) == r2.startAddress


class TestRegisterFixedSizeConstraint:
    class ElementsUnderTest:
        register_map: RegisterMap
        module: Module

        def __init__(self):
            self.register_map = RegisterMap()
            self.module = self.register_map.addModule("m1")
            self.register1 = self.module.addRegister("r1")

            self.register_under_test = self.module.addRegister("r2")

    @pytest.fixture()
    def elements_under_test(self) -> ElementsUnderTest:
        return TestRegisterFixedSizeConstraint.ElementsUnderTest()

    def test_fixed_size_ok(self, elements_under_test):
        """Show that when a fixed size constraint is applied, that the next register address is shifted appropriately."""
        assert (
            elements_under_test.module.baseAddress
            == elements_under_test.register1.startAddress
        )
        assert (
            elements_under_test.register1.startAddress + 1
        ) == elements_under_test.register_under_test.startAddress

        expected_size = 5

        elements_under_test.register1["constraints"][
            "fixedSizeMemoryUnits"
        ] = expected_size

        expected_address = (
            elements_under_test.register1.startAddress + expected_size
        )

        assert (
            expected_address
            == elements_under_test.register_under_test.startAddress
        )


class TestRegisterMemoryAlignmentConstraint:
    class ElementsUnderTest:
        def __init__(self):
            self.register_map = RegisterMap()
            self.register_map.memory.baseAddress = 0x1
            self.module = self.register_map.addModule("m1")

            self.under_test = self.module.addRegister("r1")

    @pytest.fixture()
    def elements_under_test(self) -> ElementsUnderTest():
        return TestRegisterMemoryAlignmentConstraint.ElementsUnderTest()

    def test_memory_alignment_ok(self, elements_under_test):
        """Show that address alignment shuffles a register address appropriately."""
        assert 0x1 == elements_under_test.register_map.memory.baseAddress
        assert (
            elements_under_test.register_map.memory.baseAddress
            == elements_under_test.module.baseAddress
        )
        assert (
            elements_under_test.register_map.memory.baseAddress
            == elements_under_test.under_test.startAddress
        )

        elements_under_test.under_test["constraints"][
            "alignmentMemoryUnits"
        ] = 4

        expected_value = 0x4

        assert expected_value == elements_under_test.under_test.startAddress


class TestRegisterConstraintCombinations:
    class ElementsUnderTest:
        def __init__(self):
            self.register_map = RegisterMap()
            self.module = self.register_map.addModule("m1")

            self.register_under_test = self.module.addRegister("r1")

    @pytest.fixture()
    def elements_under_test(self) -> ElementsUnderTest:
        return TestRegisterConstraintCombinations.ElementsUnderTest()

    def test_fixed_address_and_alignment_ok(self, elements_under_test):
        """Applying memory alignment to a fixed address that is memory aligned is fine (implicitly, if in future
        the fixed address is changed to an unaligned address then an exception will be raised)
        """
        elements_under_test.register_under_test["constraints"][
            "fixedAddress"
        ] = 0x4
        elements_under_test.register_under_test["constraints"][
            "alignmentMemoryUnits"
        ] = 4

    def test_fixed_address_and_alignment_raises(self, elements_under_test):
        """Show that an exception is raised when alignment conflicts with a fixed address."""
        elements_under_test.register_under_test["constraints"][
            "fixedAddress"
        ] = 0x1

        with pytest.raises(
            ConstraintError, match="^Address constraints conflict"
        ):
            elements_under_test.register_under_test["constraints"][
                "alignmentMemoryUnits"
            ] = 4

    def test_fixed_address_and_alignment_with_address_change_raises(
        self, elements_under_test
    ):
        """Show that an exception is raised when a fixed address is changed that conflicts with existing constraints."""
        elements_under_test.register_under_test["constraints"][
            "fixedAddress"
        ] = 0x4
        elements_under_test.register_under_test["constraints"][
            "alignmentMemoryUnits"
        ] = 4

        with pytest.raises(
            ConstraintError, match="^Address constraints conflict"
        ):
            elements_under_test.register_under_test["constraints"][
                "fixedAddress"
            ] = 0x5

    def test_fixed_address_and_size_ok(self, elements_under_test):
        """Show that non conflicting fixed address and fixed size constraints are okay."""
        elements_under_test.register_under_test["constraints"][
            "fixedAddress"
        ] = 0x4
        elements_under_test.register_under_test["constraints"][
            "fixedSizeMemoryUnits"
        ] = 4

    def test_alignment_and_size_ok(self, elements_under_test):
        """Show that non conflicting alignment and fixed size constraints are okay."""
        elements_under_test.register_under_test["constraints"][
            "alignmentMemoryUnits"
        ] = 4
        elements_under_test.register_under_test["constraints"][
            "fixedSizeMemoryUnits"
        ] = 4
