#  Copyright (c) 2018 Russell Smiley
#
#  This file is part of registermap.
#
#  You should have received a copy of the GNU General Public License
#  along with registermap.  If not, see <http://www.gnu.org/licenses/>.

import pytest

from registermap.registerMap import RegisterMap


@pytest.fixture()
def map_under_test() -> RegisterMap:
    return RegisterMap()


class TestRegisterMapSpan:
    def test_contiguous_register_span(self, map_under_test):
        m1 = map_under_test.addModule("m1")

        # Two byte register
        r1 = m1.addRegister("r1")
        r1.addField("f1", (0, 11))

        assert 2 == r1.sizeMemoryUnits
        expected_module_size = r1.sizeMemoryUnits
        assert expected_module_size == m1.spanMemoryUnits

        # Single byte register
        r2 = m1.addRegister("r2")
        r2.addField("f1", (0, 3))
        r2.addField("f2", (4, 6))

        assert 1 == r2.sizeMemoryUnits

        expected_module_size = r1.sizeMemoryUnits + r2.sizeMemoryUnits
        assert expected_module_size == m1.spanMemoryUnits

        assert expected_module_size == map_under_test.spanMemoryUnits

    def test_contiguous_register_span_multiple_modules(self, map_under_test):
        m1 = map_under_test.addModule("m1")

        # Two byte register
        r1 = m1.addRegister("r1")
        r1.addField("f1", (0, 11))

        assert 2 == r1.sizeMemoryUnits
        expected_module_size = r1.sizeMemoryUnits
        assert expected_module_size == m1.spanMemoryUnits

        m2 = map_under_test.addModule("m2")

        # Single byte register
        r2 = m2.addRegister("r2")
        r2.addField("f1", (0, 3))
        r2.addField("f2", (4, 6))

        assert 1 == r2.sizeMemoryUnits

        expected_m1_size = r1.sizeMemoryUnits
        assert expected_m1_size == m1.spanMemoryUnits

        expected_m2_size = r2.sizeMemoryUnits
        assert expected_m2_size == m2.spanMemoryUnits

        expected_map_size = expected_m1_size + expected_m2_size

        assert expected_map_size == map_under_test.spanMemoryUnits

    def test_discontiguous_register_span(self, map_under_test):
        m1 = map_under_test.addModule("m1")

        # Two byte register
        r1 = m1.addRegister("r1")
        r1.addField("f1", (0, 11))

        assert 2 == r1.sizeMemoryUnits
        expected_module_size = r1.sizeMemoryUnits
        assert expected_module_size == m1.spanMemoryUnits

        # Three byte register
        r2 = m1.addRegister("r2")
        r2.addField("f1", (0, 3))
        r2.addField("f2", (4, 17))

        assert 3 == r2.sizeMemoryUnits

        r2["constraints"]["fixedAddress"] = 0x15

        expected_module_size = r2.sizeMemoryUnits + r2.offset
        assert expected_module_size == m1.spanMemoryUnits

        assert expected_module_size == map_under_test.spanMemoryUnits

    def test_discontiguous_register_span_multiple_modules(self, map_under_test):
        expected_m2_address_offset = 0x15

        assert 0 == map_under_test.memory.baseAddress

        m1 = map_under_test.addModule("m1")

        # Two byte register
        r1 = m1.addRegister("r1")
        r1.addField("f1", (0, 11))

        assert 2 == r1.sizeMemoryUnits
        expected_module_size = r1.sizeMemoryUnits
        assert expected_module_size == m1.spanMemoryUnits

        m2 = map_under_test.addModule("m2")
        m2["constraints"]["fixedAddress"] = expected_m2_address_offset

        # Single byte register
        r2 = m2.addRegister("r2")
        r2.addField("f1", (0, 3))
        r2.addField("f2", (4, 6))

        assert 1 == r2.sizeMemoryUnits

        expected_m1_size = r1.sizeMemoryUnits
        assert expected_m1_size == m1.spanMemoryUnits

        expected_m2_size = r2.sizeMemoryUnits
        assert expected_m2_size == m2.spanMemoryUnits

        expected_map_size = expected_m2_address_offset + expected_m2_size

        assert expected_map_size == map_under_test.spanMemoryUnits
