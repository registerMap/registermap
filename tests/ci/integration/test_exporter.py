# Copyright (c) 2018 Russell Smiley
#
# This file is part of registerMap.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.

import io

import pytest
import yaml

from registermap.export.arguments import parseArguments
from registermap.export.cpp.registerMap import Output
from registermap.exporter import acquireRegisterMap, main
from registermap.registerMap import RegisterMap


class ParseArgumentsSideEffect:
    def __init__(self, this_mocker):
        self.mocker = this_mocker
        self.mock_generator = None
        self.options = None

    def __call__(self, command_line_arguments):
        self.options = parseArguments(command_line_arguments)

        self.mock_generator = self.mocker.create_autospec(Output)
        self.options.languageOptions.generator = self.mock_generator

        return self.options


class TestAcquireRegisterMap:
    def test_empty_register_map(self, mocker):
        mock_yaml_content = io.StringIO("")
        mocker.patch(
            "registermap.exporter.open", return_value=mock_yaml_content
        )
        actual_value = acquireRegisterMap("some/path")

        assert actual_value is None


class TestExporter:
    class ThisContext:
        def __init__(self, this_mocker):
            self.parser_side_effect = ParseArgumentsSideEffect(this_mocker)
            self.register_map = RegisterMap()

            m1 = self.register_map.addModule("m1")
            r1 = m1.addRegister("r1")
            r1.addField("f1", (0, 10))

            r2 = m1.addRegister("r2")
            r2.addField("f1", (0, 2))
            r2.addField("f2", (5, 7))

    @pytest.fixture()
    def this_context(self, mocker):
        return TestExporter.ThisContext(mocker)

    def test_exporter_sequence(self, this_context, mocker):
        input_value = [
            "registerMap/path",
            "--license-file",
            "licenseFile/text",
            "c++",
            "output/path",
        ]
        expected_license_lines = [
            "a small\n",
            "license text\n",
        ]
        input_license_text = "".join(expected_license_lines)

        mocker.patch(
            "registermap.exporter.parseArguments",
            side_effect=this_context.parser_side_effect,
        )
        mocker.patch(
            "registermap.exporter.open",
            return_value=io.StringIO(input_license_text),
        )
        mock_acquire_register_map = mocker.patch(
            "registermap.exporter.acquireRegisterMap",
            return_value=this_context.register_map,
        )
        main(input_value)

        mock_acquire_register_map.assert_called_once_with(
            this_context.parser_side_effect.options.registerMapFile
        )

        # Check that the generator was instantiated correctly.
        this_context.parser_side_effect.mock_generator.assert_called_once_with(
            this_context.parser_side_effect.options.languageOptions,
            licenseTextLines=expected_license_lines,
        )

        # Check that the generate method was called correctly.
        generator_instance = (
            this_context.parser_side_effect.mock_generator.return_value
        )
        generator_instance.generate.assert_called_once_with(
            this_context.register_map,
            this_context.parser_side_effect.options.registerMapName,
        )

    def test_license_file_none(self, this_context, mocker):
        input_value = ["registerMap/path", "c++", "output/path"]

        mocker.patch(
            "registermap.exporter.parseArguments",
            side_effect=this_context.parser_side_effect,
        )
        mock_acquire_register_map = mocker.patch(
            "registermap.exporter.acquireRegisterMap",
            return_value=this_context.register_map,
        )
        main(input_value)

        mock_acquire_register_map.assert_called_once_with(
            this_context.parser_side_effect.options.registerMapFile
        )

        mock_generator_instance = (
            this_context.parser_side_effect.mock_generator.return_value
        )
        mock_generator_instance.generate.assert_called_once_with(
            this_context.register_map,
            this_context.parser_side_effect.options.registerMapName,
        )

        # Check for the default register map name since no license file specified.
        assert (
            "registermap"
            == this_context.parser_side_effect.options.registerMapName
        )
