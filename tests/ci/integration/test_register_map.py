# Copyright 2016 Russell Smiley
#
# This file is part of registermap.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

import pytest

from registermap.registerMap import ConfigurationError, Module, RegisterMap

log = logging.getLogger(__name__)


@pytest.fixture()
def map_under_test() -> RegisterMap:
    return RegisterMap()


class TestDefaultModuleInstantiation:
    def test_default_values(self, map_under_test):
        assert map_under_test.spanMemoryUnits is None
        assert map_under_test.assignedMemoryUnits == 0


class TestBaseAddress:
    def test_default_base_address(self, map_under_test):
        expected_base_address = 0
        actual_base_address = map_under_test.memory.baseAddress

        assert isinstance(actual_base_address, int)
        assert actual_base_address == expected_base_address

    def test_set_base_address_good_value(self, map_under_test):
        expected_base_address = 25

        # Don't test with the default value
        assert expected_base_address != map_under_test.memory.baseAddress

        map_under_test.memory.baseAddress = expected_base_address
        actual_base_address = map_under_test.memory.baseAddress

        assert actual_base_address == expected_base_address


class TestMemoryAddress:
    def test_default_memory_address_bits(self, map_under_test):
        expected_memory_address_bits = 32
        actual_memory_address_bits = map_under_test.memory.addressBits

        assert isinstance(actual_memory_address_bits, int)
        assert actual_memory_address_bits == expected_memory_address_bits

    def test_set_good_value(self, map_under_test):
        expected_memory_address_bits = 20

        assert expected_memory_address_bits != map_under_test.memory.addressBits

        map_under_test.memory.addressBits = expected_memory_address_bits
        actual_memory_address_bits = map_under_test.memory.addressBits

        assert actual_memory_address_bits == expected_memory_address_bits


class TestMemoryUnit:
    def test_default_memory_unit_bits(self, map_under_test):
        expected_memory_unit_bits = 8
        actual_memory_unit_bits = map_under_test.memory.memoryUnitBits

        assert isinstance(actual_memory_unit_bits, int)
        assert actual_memory_unit_bits == expected_memory_unit_bits


class TestPageSize:
    def test_default_page_size(self, map_under_test):
        actual_page_size = map_under_test.memory.pageSize

        assert actual_page_size is None

    def test_page_size_assignment(self, map_under_test):
        expected_page_size = 128

        assert expected_page_size != map_under_test.memory.pageSize

        map_under_test.memory.pageSize = expected_page_size
        actual_page_size = map_under_test.memory.pageSize

        assert actual_page_size == expected_page_size


class TestRegisterMapModules:
    def test_default_modules(self, map_under_test):
        assert isinstance(map_under_test["modules"], dict)
        assert len(map_under_test["modules"]) == 0


class TestAddModule:
    def test_add_single_module(self, map_under_test):
        assert map_under_test.spanMemoryUnits is None
        assert 0 == map_under_test.assignedMemoryUnits
        assert 0 == len(map_under_test["modules"])

        new_module = map_under_test.addModule("m1")

        assert 0 == map_under_test.spanMemoryUnits
        assert 0 == map_under_test.assignedMemoryUnits
        assert 1 == len(map_under_test["modules"])
        assert isinstance(new_module, Module)

        assert new_module.baseAddress == map_under_test.memory.baseAddress

    def test_add_duplicate_module_name_raises(self, map_under_test):
        assert map_under_test.spanMemoryUnits is None
        assert 0 == map_under_test.assignedMemoryUnits
        assert 0 == len(map_under_test["modules"])

        map_under_test.addModule("m1")

        with pytest.raises(
            ConfigurationError,
            match="^Created module names must be unique within a register map",
        ):
            map_under_test.addModule("m1")


class TestModuleAddresses:
    def test_base_address_update(self, map_under_test):
        assert map_under_test.spanMemoryUnits is None
        assert 0 == map_under_test.assignedMemoryUnits
        assert 0 == len(map_under_test["modules"])

        m1 = map_under_test.addModule("m1")
        assert 0 == map_under_test.spanMemoryUnits
        assert 0 == map_under_test.assignedMemoryUnits
        assert 1 == len(map_under_test["modules"])
        assert map_under_test.memory.baseAddress == m1.baseAddress

        r1 = m1.addRegister("r1")
        assert 1 == map_under_test.spanMemoryUnits
        assert 1 == map_under_test.assignedMemoryUnits
        assert 1 == len(map_under_test["modules"])
        assert map_under_test.memory.baseAddress == r1.startAddress

        map_under_test.memory.baseAddress = 0x10

        # Existing register and module must reflect the base address change.
        assert map_under_test.memory.baseAddress == m1.baseAddress
        assert map_under_test.memory.baseAddress == r1.startAddress

    def test_module_sequential_addresses(self, map_under_test):
        assert map_under_test.spanMemoryUnits is None
        assert 0 == map_under_test.assignedMemoryUnits
        assert 0 == len(map_under_test["modules"])

        m1 = map_under_test.addModule("m1")
        assert 0 == map_under_test.spanMemoryUnits
        assert 0 == map_under_test.assignedMemoryUnits
        assert 1 == len(map_under_test["modules"])
        assert map_under_test.memory.baseAddress == m1.baseAddress

        r1 = m1.addRegister("r1")
        assert 1 == map_under_test.spanMemoryUnits
        assert 1 == map_under_test.assignedMemoryUnits
        assert 1 == len(map_under_test["modules"])
        assert map_under_test.memory.baseAddress == r1.startAddress

        m2 = map_under_test.addModule("m2")
        r2 = m2.addRegister("r2")

        map_under_test.memory.baseAddress = 0x10

        # Existing registers and modules must reflect the base address change.
        assert map_under_test.memory.baseAddress == m1.baseAddress
        assert map_under_test.memory.baseAddress == r1.startAddress
        assert (map_under_test.memory.baseAddress + 1) == m2.baseAddress
        assert (map_under_test.memory.baseAddress + 1) == r2.startAddress

    def test_assigned_count(self, map_under_test):
        assert 8 == map_under_test.memory.memoryUnitBits
        assert map_under_test.spanMemoryUnits is None
        assert 0 == map_under_test.assignedMemoryUnits
        assert 0 == len(map_under_test["modules"])

        m1 = map_under_test.addModule("m1")
        r1 = m1.addRegister("r1")
        m2 = map_under_test.addModule("m2")
        r2 = m2.addRegister("r2")
        r2.addField("f2", [3, 10], (3, 10))
        r2["constraints"]["fixedAddress"] = 0x15

        log.debug("m1 address before base assigment: " + hex(m1.baseAddress))
        log.debug("m2 address before base assigment: " + hex(m2.baseAddress))
        log.debug("r1 address before base assigment: " + hex(r1.startAddress))
        log.debug("r2 address before base assigment: " + hex(r2.startAddress))

        assert 3 == map_under_test.assignedMemoryUnits
        assert 23 == map_under_test.spanMemoryUnits

        log.debug("register map base address assignment")
        map_under_test.memory.baseAddress = 0x10

        log.debug("m1 address after base assigment: " + hex(m1.baseAddress))
        log.debug("m2 address after base assigment: " + hex(m2.baseAddress))
        log.debug("r1 address after base assigment: " + hex(r1.startAddress))
        log.debug("r2 address after base assigment: " + hex(r2.startAddress))

        assert 1 == m1.spanMemoryUnits
        assert 6 == m2.spanMemoryUnits

        expected_register_map_span = m1.spanMemoryUnits + m2.spanMemoryUnits

        assert 3 == map_under_test.assignedMemoryUnits
        assert expected_register_map_span == map_under_test.spanMemoryUnits
