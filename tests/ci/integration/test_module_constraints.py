#
# Copyright (c) 2018 Russell Smiley
#
# This file is part of registerMap.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.exceptions import ConstraintError
from registermap.registerMap import RegisterMap


class TestModuleFixedAddressConstraint:
    class ElementsUnderTest:
        def __init__(self):
            self.register_map = RegisterMap()
            self.module_under_test = self.register_map.addModule("m1")

    @pytest.fixture()
    def elements_under_test(self) -> ElementsUnderTest:
        return TestModuleFixedAddressConstraint.ElementsUnderTest()

    def test_fixed_address_ok(self, elements_under_test):
        assert 0x0 == elements_under_test.register_map.memory.baseAddress
        assert (
            elements_under_test.register_map.memory.baseAddress
            == elements_under_test.module_under_test.baseAddress
        )

        expected_value = 0x20F0

        elements_under_test.module_under_test["constraints"][
            "fixedAddress"
        ] = expected_value

        # The register map base address is unchanged.
        assert 0x0 == elements_under_test.register_map.memory.baseAddress
        # The module has the specified base address.
        assert (
            expected_value == elements_under_test.module_under_test.baseAddress
        )


class TestModuleFixedSizeConstraint:
    class ElementsUnderTest:
        def __init__(self):
            self.register_map = RegisterMap()
            self.module_under_test = self.register_map.addModule("m1")

    @pytest.fixture()
    def elements_under_test(self):
        return TestModuleFixedSizeConstraint.ElementsUnderTest()

    def test_fixed_size_ok(self, elements_under_test):
        assert 0 == elements_under_test.module_under_test.spanMemoryUnits

        expected_value = 10

        elements_under_test.module_under_test["constraints"][
            "fixedSizeMemoryUnits"
        ] = expected_value

        assert (
            expected_value
            == elements_under_test.module_under_test.spanMemoryUnits
        )

    def test_exceeded_size_register_add_raises(self, elements_under_test):
        module_size = 3
        elements_under_test.module_under_test["constraints"][
            "fixedSizeMemoryUnits"
        ] = module_size

        for i in range(0, module_size):
            elements_under_test.module_under_test.addRegister("r{0}".format(i))

        # Attempting to add a fourth register to a module constrained to size 3 raises an exception.
        with pytest.raises(ConstraintError, match="^Fixed size exceeded"):
            elements_under_test.module_under_test.addRegister("r3")

    def test_exceeded_size_constraint_too_small_raises(
        self, elements_under_test
    ):
        module_size = 3

        for i in range(0, module_size):
            elements_under_test.module_under_test.addRegister("r{0}".format(i))

        assert (
            module_size == elements_under_test.module_under_test.spanMemoryUnits
        )

        # Attempting to constrain the size smaller than the allocated registers raises
        with pytest.raises(ConstraintError, match="^Fixed size exceeded"):
            elements_under_test.module_under_test["constraints"][
                "fixedSizeMemoryUnits"
            ] = (module_size - 1)


class TestModuleMemoryAlignmentConstraint:
    class ElementsUnderTest:
        def __init__(self):
            self.register_map = RegisterMap()
            # Assign an unaligned base address to the map.
            self.register_map.memory.baseAddress = 0x1
            self.module_under_test = self.register_map.addModule("m1")

    @pytest.fixture()
    def elements_under_test(self):
        return TestModuleMemoryAlignmentConstraint.ElementsUnderTest()

    def testWordAlignedAddress(self, elements_under_test):
        assert 8 == elements_under_test.register_map.memory.memoryUnitBits
        assert (
            elements_under_test.register_map.memory.baseAddress
            == elements_under_test.module_under_test.baseAddress
        )

        # Align the module start address to 32 bit (8 bits * 4 memory units).
        elements_under_test.module_under_test["constraints"][
            "alignmentMemoryUnits"
        ] = 4

        assert 0x4 == elements_under_test.module_under_test.baseAddress


class TestModuleConstraintCombinations:
    class ElementsUnderTest:
        def __init__(self):
            self.register_map = RegisterMap()
            self.module_under_test = self.register_map.addModule("m1")

    @pytest.fixture()
    def elements_under_test(self):
        return TestModuleConstraintCombinations.ElementsUnderTest()

    def test_fixed_address_and_alignment_ok(self, elements_under_test):
        """Applying memory alignment to a fixed address that is memory aligned
        is fine.

        Implicitly, if in future the fixed address is changed to an unaligned
        address then an exception will be raised
        """
        elements_under_test.module_under_test["constraints"][
            "fixedAddress"
        ] = 0x4
        elements_under_test.module_under_test["constraints"][
            "alignmentMemoryUnits"
        ] = 4

    def test_fixed_address_and_alignment_raises(self, elements_under_test):
        elements_under_test.module_under_test["constraints"][
            "fixedAddress"
        ] = 0x1

        with pytest.raises(
            ConstraintError, match="^Address constraints conflict"
        ):
            elements_under_test.module_under_test["constraints"][
                "alignmentMemoryUnits"
            ] = 4

    def test_fixed_address_and_alignment_with_address_change_raises(
        self, elements_under_test
    ):
        elements_under_test.module_under_test["constraints"][
            "fixedAddress"
        ] = 0x4
        elements_under_test.module_under_test["constraints"][
            "alignmentMemoryUnits"
        ] = 4

        with pytest.raises(
            ConstraintError, match="^Address constraints conflict"
        ):
            elements_under_test.module_under_test["constraints"][
                "fixedAddress"
            ] = 0x5

    def test_fixed_address_and_size_ok(self, elements_under_test):
        elements_under_test.module_under_test["constraints"][
            "fixedAddress"
        ] = 0x4
        elements_under_test.module_under_test["constraints"][
            "fixedSizeMemoryUnits"
        ] = 4

    def test_alignment_and_size_ok(self, elements_under_test):
        elements_under_test.module_under_test["constraints"][
            "alignmentMemoryUnits"
        ] = 4
        elements_under_test.module_under_test["constraints"][
            "fixedSizeMemoryUnits"
        ] = 4
