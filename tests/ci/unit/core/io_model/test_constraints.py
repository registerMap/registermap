#  Copyright (c) 2023 Russell Smiley
#
#  This file is part of registermap.
#
#  You should have received a copy of the GNU General Public License
#  along with registermap.  If not, see <http://www.gnu.org/licenses/>.

import pytest

from registermap.core.io_model.constraints import ConstraintCollection


class TestConstraintCollection:
    def test_default(self):
        under_test = ConstraintCollection()

        result = under_test.model_dump()

        assert result == {
            "fixed_address": None,
            "fixed_size": None,
            "memory_alignment": None,
        }

    def test_values(self):
        under_test = ConstraintCollection(
            fixed_address=1, fixed_size=2, memory_alignment=3
        )

        result = under_test.model_dump()

        assert result == {
            "fixed_address": 1,
            "fixed_size": 2,
            "memory_alignment": 3,
        }

    def test_from_dict(self):
        this_input = {
            "fixed_address": 1,
            "fixed_size": 2,
            "memory_alignment": 3,
        }

        under_test = ConstraintCollection.model_validate(this_input)

        assert under_test.fixed_address == 1
        assert under_test.fixed_size == 2
        assert under_test.memory_alignment == 3
