#  Copyright (c) 2023 Russell Smiley
#
#  This file is part of registermap.
#
#  You should have received a copy of the GNU General Public License
#  along with registermap.  If not, see <http://www.gnu.org/licenses/>.

import pytest

from registermap.core.io_model._default_values import (
    DEFAULT_ADDRESS_BITS,
    DEFAULT_BASE_ADDRESS,
    DEFAULT_MEMORY_UNIT_BITS,
    DEFAULT_PAGE_SIZE_MEMORY_UNITS,
)
from registermap.core.io_model.memory import MemoryConfiguration


class TestMemoryConfiguration:
    def test_default(self):
        under_test = MemoryConfiguration()

        assert under_test.address_bits == DEFAULT_ADDRESS_BITS
        assert under_test.base_address == DEFAULT_BASE_ADDRESS
        assert under_test.memory_unit_bits == DEFAULT_MEMORY_UNIT_BITS
        assert under_test.page_size == DEFAULT_PAGE_SIZE_MEMORY_UNITS

    def test_values(self):
        under_test = MemoryConfiguration(
            address_bits=8,
            base_address=0x400,
            memory_unit_bits=12,
            page_size=128,
        )

        assert under_test.address_bits == 8
        assert under_test.base_address == 0x400
        assert under_test.memory_unit_bits == 12
        assert under_test.page_size == 128

    def test_from_dict(self):
        this_input = {
            "address_bits": 8,
            "base_address": 0x400,
            "memory_unit_bits": 12,
            "page_size": 128,
        }

        under_test = MemoryConfiguration.model_validate(this_input)

        assert under_test.address_bits == 8
        assert under_test.base_address == 0x400
        assert under_test.memory_unit_bits == 12
        assert under_test.page_size == 128
