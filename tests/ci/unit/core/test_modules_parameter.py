#
# Copyright 2017 Russell Smiley
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

import registermap.registerMap as rm
import registermap.structure.elements.base as rmbs
import registermap.structure.memory.configuration as rmm
from registermap.structure.set import SetCollection


class ElementsContext:
    memory_space: rmm.MemoryConfiguration
    register_map: rm.RegisterMap

    acquired_field_set: SetCollection
    source_field_set: SetCollection

    def __init__(self) -> None:
        self.memory_space = rmm.MemoryConfiguration()
        self.register_map = rm.RegisterMap()
        self.source_field_set = SetCollection()

        self.acquired_field_set = SetCollection()

    def create_sample_module(self, name) -> rm.Module:
        module = rm.Module(self.memory_space, self.source_field_set)

        module["name"] = name

        register = module.addRegister("r1")

        register.addField("f1", [3, 5], (3, 5))
        register.addField("f2", [7, 7], (7, 7))

        return module


@pytest.fixture()
def define_context() -> ElementsContext:
    return ElementsContext()


class TestModulesParameter:
    def test_initialization(self, define_context: ElementsContext):
        p = rm.ModulesParameter(define_context.register_map)

        assert p.name == "modules"
        assert isinstance(p.value, rmbs.ElementList)

    def test_empty_yaml_load_save(self, define_context: ElementsContext):
        p = rm.ModulesParameter(define_context.register_map)

        generated_yaml_data = p.to_yamlData()
        decoded_modules = rm.ModulesParameter.from_yamlData(
            generated_yaml_data,
            define_context.register_map,
            define_context.memory_space,
            define_context.acquired_field_set,
        )

        assert len(decoded_modules.value) == 0

    def test_single_module_yaml_load_save(
        self, define_context: ElementsContext
    ):
        module = define_context.create_sample_module("m1")

        p = rm.ModulesParameter(define_context.register_map)
        p.value[module["name"]] = module

        generated_yaml_data = p.to_yamlData()
        decoded_modules = rm.ModulesParameter.from_yamlData(
            generated_yaml_data,
            define_context.register_map,
            define_context.memory_space,
            define_context.acquired_field_set,
        )

        assert (
            decoded_modules.value["m1"]["description"]
            == p.value["m1"]["description"]
        )
        assert decoded_modules.value["m1"]["name"] == p.value["m1"]["name"]
        assert (
            decoded_modules.value["m1"]["summary"] == p.value["m1"]["summary"]
        )

    def test_from_optional_yaml_data(self, define_context: ElementsContext):
        yaml_data = {"mode": "ro"}
        decoded_modules = rm.ModulesParameter.from_yamlData(
            yaml_data,
            define_context.register_map,
            define_context.memory_space,
            define_context.acquired_field_set,
            optional=True,
        )

        assert len(decoded_modules.value) == 0
