#
# Copyright 2016 Russell Smiley
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.core.observable import Observable
from registermap.core.observer import Observer


@pytest.fixture()
def observable_under_test() -> Observable:
    return Observable()


class TestObservable:
    def test_construct(self, observable_under_test):
        this_observable = Observable()

        assert this_observable.observers == []


class TestAddRemoveObservers:
    class MockObserver(Observer):
        def update(self, observable, arguments):
            pass

    def test_add_observer(self, observable_under_test):
        expected_observer = TestAddRemoveObservers.MockObserver()

        observable_under_test.addObserver(expected_observer)

        assert len(observable_under_test.observers) == 1
        assert observable_under_test.observers[0] == expected_observer

    def test_remove_observer(self, observable_under_test):
        expected_observer = TestAddRemoveObservers.MockObserver()

        observable_under_test.addObserver(expected_observer)

        assert len(observable_under_test.observers) == 1
        assert observable_under_test.observers[0] == expected_observer

        observable_under_test.removeObserver(expected_observer)

        assert len(observable_under_test.observers) == 0

    def test_remove_observers(self, observable_under_test):
        expected_number_observers = 4
        for i in range(0, expected_number_observers):
            observable_under_test.addObserver(
                TestAddRemoveObservers.MockObserver()
            )

        assert len(observable_under_test.observers) == expected_number_observers

        observable_under_test.removeObservers()

        assert len(observable_under_test.observers) == 0


class TestNotifyObservers:
    class MockObserver(Observer):
        def __init__(self):
            self.update_count = 0

        def update(self, source, arguments):
            self.update_count += 1

        @property
        def updated(self):
            return_value = False
            if self.update_count != 0:
                return_value = True

            return return_value

    def test_single_notification(self, observable_under_test):
        expected_observer = TestNotifyObservers.MockObserver()

        observable_under_test.addObserver(expected_observer)

        assert len(observable_under_test.observers) == 1
        assert observable_under_test.observers[0] == expected_observer

        observable_under_test.notifyObservers()

        assert expected_observer.updated
