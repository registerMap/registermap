#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.core.parameters import ElementsParameter
from registermap.structure.elements.module import Module
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set import SetCollection


class ElementsContext:
    def __init__(self) -> None:
        self.memory_space = MemoryConfiguration()
        self.set_collection = SetCollection()

        self.setup_set_collection()

        self.elements_for_test = ElementsParameter(self.set_collection)

    def create_module(self, name):
        this_module = Module(self.memory_space, self.set_collection)
        this_module["name"] = name

        self.set_collection.moduleSet.add(this_module)

        return this_module

    def setup_set_collection(self) -> None:
        m = self.create_module("module")

        r = m.addRegister("register")

        lf = r.addField("local-field", [0, 3])

        assert "module.register.local-field" == lf.canonicalId

        gf = r.addField("global-field", [5, 6], isGlobal=True)
        assert "global-field" == gf.canonicalId


@pytest.fixture()
def define_context() -> ElementsContext:
    return ElementsContext()


class TestElementsParameter:
    def test_global_field_found(self, define_context):
        actual_element = define_context.elements_for_test["global-field"]

        assert actual_element is not None
        assert actual_element["name"] == "global-field"

    def test_local_field_found(self, define_context):
        actual_element = define_context.elements_for_test[
            "module.register.local-field"
        ]

        assert actual_element is not None
        assert actual_element["name"] == "local-field"

    def test_register_found(self, define_context):
        actual_element = define_context.elements_for_test["module.register"]

        assert actual_element is not None
        assert actual_element["name"] == "register"

    def test_module_found(self, define_context):
        actual_element = define_context.elements_for_test["module"]

        assert actual_element is not None
        assert actual_element["name"] == "module"

    def test_element_not_found(self, define_context):
        actual_element = define_context.elements_for_test["not-an-element"]

        assert actual_element is None
