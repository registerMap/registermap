#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.core.interval import ClosedIntegerInterval, sort_intervals


class TestIntervalSort:
    def test_sort_interval_list(self):
        input_value = [
            ClosedIntegerInterval((3, 4)),
            ClosedIntegerInterval((0, 2)),
            ClosedIntegerInterval((5, 7)),
        ]

        actual_value = sort_intervals(input_value)

        expected_value = [
            ClosedIntegerInterval((0, 2)),
            ClosedIntegerInterval((3, 4)),
            ClosedIntegerInterval((5, 7)),
        ]

        assert expected_value == actual_value

    def test_sort_contiguous_interval_set(self):
        input_value = {
            ClosedIntegerInterval((3, 4)),
            ClosedIntegerInterval((0, 2)),
            ClosedIntegerInterval((5, 7)),
        }

        actual_value = sort_intervals(input_value)

        expected_value = [
            ClosedIntegerInterval((0, 2)),
            ClosedIntegerInterval((3, 4)),
            ClosedIntegerInterval((5, 7)),
        ]

        assert expected_value == actual_value

    def test_sort_noncontiguous_interval_set(self):
        input_value = {
            ClosedIntegerInterval((4, 4)),
            ClosedIntegerInterval((0, 1)),
            ClosedIntegerInterval((6, 7)),
        }

        actual_value = sort_intervals(input_value)

        expected_value = [
            ClosedIntegerInterval((0, 1)),
            ClosedIntegerInterval((4, 4)),
            ClosedIntegerInterval((6, 7)),
        ]

        assert expected_value == actual_value

    def test_overlapping_interval_asserts(self):
        input_value = {
            ClosedIntegerInterval((3, 5)),
            ClosedIntegerInterval((0, 2)),
            ClosedIntegerInterval((5, 7)),
        }

        with pytest.raises(AssertionError):
            sort_intervals(input_value)
