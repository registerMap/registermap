#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.core.interval import ClosedIntegerInterval, IntervalError


class TestIntervalDefaultConstructor:
    def test_default_value(self):
        r = ClosedIntegerInterval()
        assert r == None

    def test_constructed_value(self):
        expected_value = {3, 6}
        r = ClosedIntegerInterval(value=expected_value)
        assert r.value == expected_value

    def test_hashable(self):
        # Using ClosedIntegerInterval as key in a dict must not fail.
        r = ClosedIntegerInterval()
        x = dict()
        x[r] = "test"

    def test_hash_two_equal_intervals_is_same(self):
        interval = (4, 7)
        r1 = ClosedIntegerInterval()
        r1.value = interval

        r2 = ClosedIntegerInterval()
        r2.value = interval

        assert hash(r1) == hash(r2)


class TestIntervalAssignment:
    @pytest.fixture()
    def this_context(self):
        self.under_test = ClosedIntegerInterval()

    def test_bad_constructed_value_raises(self, this_context):
        expected_value = "[3, 6]"
        with pytest.raises(
            IntervalError,
            match="^Interval must be a set of one or two positive integers",
        ):
            ClosedIntegerInterval(value=expected_value)

    def test_set_value_assignment(self, this_context):
        expected_value = {0, 8}

        self.under_test.value = expected_value

        assert isinstance(self.under_test, ClosedIntegerInterval)
        assert self.under_test == expected_value

    def test_list_value_assignment(self, this_context):
        expected_value = [0, 8]

        self.under_test.value = expected_value

        assert isinstance(self.under_test, ClosedIntegerInterval)
        assert self.under_test == expected_value

    def test_tuple_value_assignment(self, this_context):
        expected_value = (0, 8)

        self.under_test.value = expected_value

        assert isinstance(self.under_test, ClosedIntegerInterval)
        assert self.under_test == expected_value

    def test_set_single_value_assignment(self, this_context):
        expected_value = {1}

        self.under_test.value = expected_value

        assert isinstance(self.under_test, ClosedIntegerInterval)
        assert self.under_test == expected_value

    def test_list_single_value_assignment(self, this_context):
        expected_value = [1, 1]

        self.under_test.value = expected_value

        assert isinstance(self.under_test, ClosedIntegerInterval)
        assert self.under_test == expected_value

    def test_tuple_single_value_assignment(self, this_context):
        expected_value = (1, 1)

        self.under_test.value = expected_value

        assert isinstance(self.under_test, ClosedIntegerInterval)
        assert self.under_test == expected_value

    def test_non_list_raises(self, this_context):
        with pytest.raises(
            IntervalError,
            match="^Interval must be a set of one or two positive integers",
        ):
            self.under_test.value = "5"

    def test_list_non_int_raises(self, this_context):
        with pytest.raises(
            IntervalError,
            match="^Interval must be a set of one or two positive integers",
        ):
            self.under_test.value = [5, "6"]

    def test_negative_values_raises(self, this_context):
        with pytest.raises(
            IntervalError,
            match="^Interval must be a set of one or two positive integers",
        ):
            self.under_test.value = [5, -7]

    def test_wrong_length_raises(self, this_context):
        with pytest.raises(
            IntervalError,
            match="^Interval must be a set of one or two positive integers",
        ):
            self.under_test.value = [5, 7, 6]

        with pytest.raises(
            IntervalError,
            match="^Interval must be a set of one or two positive integers",
        ):
            self.under_test.value = tuple()


class TestIntervalSize:
    @pytest.fixture()
    def this_context(self):
        self.under_test = ClosedIntegerInterval()

    def test_default_size(self, this_context):
        assert self.under_test.size == 0

    def test_size_single(self, this_context):
        self.under_test.value = (5, 5)
        assert self.under_test.size == 1

    def test_size_multiple(self, this_context):
        self.under_test.value = (5, 8)
        assert self.under_test.size == 4


class TestEqualOperator:
    @pytest.fixture()
    def this_context(self):
        self.expected_value = [3, 7]
        self.under_test = ClosedIntegerInterval(value=self.expected_value)

    def test_equal_set(self, this_context):
        assert self.under_test == {3, 7}

    def test_equal_list(self, this_context):
        assert self.under_test == [3, 7]

    def test_equal_tuple(self, this_context):
        assert self.under_test == (3, 7)

    def test_none(self, this_context):
        assert not self.under_test == None

    def test_bit_range(self, this_context):
        assert self.under_test == ClosedIntegerInterval(
            value=self.expected_value
        )
        assert not self.under_test == ClosedIntegerInterval(value=[0, 4])


class TestNotEqualOperator:
    @pytest.fixture()
    def this_context(self):
        self.expected_value = [3, 7]
        self.under_test = ClosedIntegerInterval(value=self.expected_value)

    def test_list(self, this_context):
        assert self.under_test != [0, 4]

    def test_none(self, this_context):
        assert self.under_test != None

    def test_bit_range(self, this_context):
        assert not self.under_test != ClosedIntegerInterval(
            value=self.expected_value
        )
        assert self.under_test != ClosedIntegerInterval(value=[0, 4])


class TestIntegerOffsetArithmetic:
    @pytest.fixture()
    def this_context(self):
        self.expected_value = [3, 7]
        self.under_test = ClosedIntegerInterval(value=self.expected_value)

    def test_add_integer(self, this_context):
        expected_result = ClosedIntegerInterval(value=(6, 10))
        actual_result = self.under_test + 3

        assert actual_result == expected_result

    def test_integer_add_in_place(self, this_context):
        expected_result = ClosedIntegerInterval(value=(6, 10))
        self.under_test += 3

        assert self.under_test == expected_result

    def test_subtract_integer(self, this_context):
        expected_result = ClosedIntegerInterval(value=(0, 4))
        actual_result = self.under_test - 3

        assert actual_result == expected_result

    def test_integer_subtract_in_place(self, this_context):
        expected_result = ClosedIntegerInterval(value=(0, 4))
        self.under_test -= 3

        assert self.under_test == expected_result
