#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.elements.field import Field
from registermap.structure.elements.register import RegisterInstance
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set.fieldSet import ConfigurationError, FieldSet


class ElementsContext:
    def __init__(self):
        self.memory = MemoryConfiguration()

        self.set_under_test = FieldSet()

        self.register = RegisterInstance(
            self.memory, setCollection=self.set_under_test
        )


@pytest.fixture()
def elements_context() -> ElementsContext:
    return ElementsContext()


class TestAddField:
    def test_add_single_field_okay(self, elements_context: ElementsContext):
        """
        When: a first bit field with the default name is added Then: the size of the bit field set is 1
        """
        b = Field(parent=elements_context.register)

        assert len(elements_context.set_under_test) == 0

        elements_context.set_under_test.add(b)

        assert len(elements_context.set_under_test) == 1

    def test_add_non_global_fields_okay(
        self, elements_context: ElementsContext
    ):
        """
        When: a second bit field is added And: the new bit field has the same name as the first bit field And: the new bit field is defined as non-global And: the first bit field is defined as non-global Then: the size of the bit field set is 2
        """
        b1 = Field(parent=elements_context.register)
        b2 = Field(parent=elements_context.register)

        assert len(elements_context.set_under_test) == 0

        elements_context.set_under_test.add(b1)
        elements_context.set_under_test.add(b2)

        assert len(elements_context.set_under_test) == 2

    def test_add_global_fields_raises(self, elements_context: ElementsContext):
        """
        When: a second bit field is added
        And: the new bit field has the same name as the first bit field
        And: the new bit field is defined as global
        And: the first bit field is defined as global
        Then: an exception is raised
        """
        b1 = Field()

        b2 = Field()

        assert len(elements_context.set_under_test) == 0

        elements_context.set_under_test.add(b1)

        with pytest.raises(
            ConfigurationError,
            match="^Only one global field of a name can exist",
        ):
            elements_context.set_under_test.add(b2)

    def test_add_global_field_non_global_bit_field_okay(
        self, elements_context: ElementsContext
    ):
        """
        When: a second bit field is added And: the new bit field has the same name as the first bit field And: the new bit field is defined as global And: the first bit field is defined as non-global Then: the size of the bit field set is 2
        """
        b1 = Field(parent=elements_context.register)
        assert not b1["global"]

        b2 = Field()
        assert b2["global"]

        assert len(elements_context.set_under_test) == 0

        elements_context.set_under_test.add(b1)
        elements_context.set_under_test.add(b2)

        assert len(elements_context.set_under_test) == 2

    def test_add_non_global_field_global_bit_field_okay(
        self, elements_context: ElementsContext
    ):
        """
        When: a second bit field is added And: the new bit field has the same name as the first bit field And: the new bit field is defined as non-global And: the first bit field is defined as global Then: the size of the bit field set is 2
        """
        b1 = Field()

        b2 = Field(parent=elements_context.register)

        assert len(elements_context.set_under_test) == 0

        elements_context.set_under_test.add(b1)
        elements_context.set_under_test.add(b2)

        assert len(elements_context.set_under_test) == 2


class TestFieldSetMultipleElements:
    @pytest.fixture()
    def populate(self, elements_context):
        self.number_unique_elements = 4
        for i in range(0, self.number_unique_elements):
            f = Field()
            f["name"] = repr(i)

            elements_context.set_under_test.add(f)

        self.number_repeated_elements = 3
        elements_context.registers = list()
        for i in range(0, self.number_repeated_elements):
            elements_context.registers.append(
                RegisterInstance(
                    elements_context.memory,
                    setCollection=elements_context.set_under_test,
                )
            )
            f = Field(parent=elements_context.registers[i])
            f["name"] = "a"

            elements_context.set_under_test.add(f)

    def test_find_multiple_items(
        self, elements_context: ElementsContext, populate
    ):
        # Look for the items known to be in the set.
        actual_items_found = elements_context.set_under_test.find("a")

        assert len(elements_context.set_under_test) == (
            self.number_repeated_elements + self.number_unique_elements
        )
        assert len(actual_items_found) == self.number_repeated_elements
