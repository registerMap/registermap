#
# Copyright 2023 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import typing

from registermap.structure.set.elementSet import ElementSet
from tests.ci.unit.structure.set.utility import SetTestBase


class MockElement(object):
    def __init__(self):
        self.__data = dict()

    def __getitem__(self, item):
        return self.__data[item]

    def __setitem__(self, item, value):
        self.__data[item] = value


class ElementSetTestBase(SetTestBase[ElementSet, MockElement]):
    def __init__(self):
        super().__init__()

    @staticmethod
    def get_set_type() -> typing.Type[ElementSet]:
        return ElementSet

    @staticmethod
    def get_element_type() -> typing.Type[MockElement]:
        return MockElement
