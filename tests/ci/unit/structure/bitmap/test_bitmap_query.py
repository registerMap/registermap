#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.bitmap.bitmap import BitMap, BitRange

from .mocks import MockBitStore


class TestQueryRanges:
    @pytest.fixture()
    def this_context(self):
        self.source_size = 16
        self.source_id = "testSource"
        self.mock_source = MockBitStore(self.source_id, self.source_size)
        self.source_bit_map = BitMap(self.mock_source)
        self.mock_source.bitMap = self.source_bit_map

        self.destination_size = 4
        self.destination_id = "testDestination"
        self.mock_destination = MockBitStore(
            self.destination_id, self.destination_size
        )
        self.destination_bit_map = BitMap(self.mock_destination)
        self.mock_destination.bitMap = self.destination_bit_map

    def test_assigned_source_range_persists(self, this_context):
        # The assigned source range can be queried.

        field_range = BitRange((0, (self.destination_size - 1)))
        register_bit_offset = 4
        register_range = BitRange(
            (
                register_bit_offset,
                (register_bit_offset + self.destination_size - 1),
            )
        )

        self.source_bit_map.mapBits(
            register_range, field_range, self.mock_destination
        )

        actual_intervals = self.source_bit_map.sourceIntervals

        assert len(actual_intervals) == 1
        actual_value = actual_intervals.pop()

        assert actual_value == register_range

    def test_assigned_destination_range_persists(self, this_context):
        # The assigned destination range can be queried.

        field_range = BitRange((0, (self.destination_size - 1)))
        register_bit_offset = 4
        register_range = BitRange(
            (
                register_bit_offset,
                (register_bit_offset + self.destination_size - 1),
            )
        )

        self.source_bit_map.mapBits(
            register_range, field_range, self.mock_destination
        )

        actual_intervals = self.source_bit_map.destinationIntervals

        assert len(actual_intervals) == 1
        (source_interval, actual_value) = actual_intervals.popitem()

        assert actual_value == field_range
        assert source_interval == register_range

    def test_assigned_ranges_persists(self, this_context):
        # The assigned destination range can be queried.

        field_range = BitRange((0, (self.destination_size - 1)))
        register_bit_offset = 4
        register_range = BitRange(
            (
                register_bit_offset,
                (register_bit_offset + self.destination_size - 1),
            )
        )

        self.source_bit_map.mapBits(
            register_range, field_range, self.mock_destination
        )

        actual_intervals = self.source_bit_map.intervalMap

        assert len(actual_intervals) == 1
        (source_range, destination_value) = actual_intervals.popitem()

        assert destination_value["destination"] == self.mock_destination
        assert source_range == register_range
        assert destination_value["interval"] == field_range
