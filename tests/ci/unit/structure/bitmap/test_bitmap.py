#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.bitmap import BitMap

from .mocks import MockBitStore


class TestBitMapRegisterSize:
    @pytest.fixture()
    def this_context(self):
        self.source_size = 16
        self.source_id = "testSource"
        self.mock_source = MockBitStore(self.source_id, self.source_size)

    def test_register_size(self, this_context):
        under_test = BitMap(self.mock_source)

        assert under_test.source == self.mock_source
