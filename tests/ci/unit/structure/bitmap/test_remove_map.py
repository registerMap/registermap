#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.bitmap.bitmap import BitMap, ConfigurationError
from registermap.structure.bitrange import BitRange
from registermap.structure.elements.field import Field
from registermap.structure.elements.register import RegisterInstance


class TestBitMapRemoveMap:
    @pytest.fixture()
    def this_context(self, mocker):
        self.mock_register = mocker.create_autospec(RegisterInstance)
        self.mock_field = mocker.create_autospec(Field)

        self.under_test = BitMap(self.mock_register)

    def test_neither_destination_nor_interval_asserts(self, this_context):
        with pytest.raises(AssertionError):
            self.under_test.removeDestination()

    def test_remove_destination(self, this_context, mocker):
        def check_removed_from_source_mapping():
            nonlocal self

            destination_mappings = self.under_test.intervalMap.values()
            for k in destination_mappings:
                assert k["destination"] != self.mock_field

        # Removing an entire destination object also removes the corresponding source intervals.
        br = mocker.patch.object(self.under_test, "_BitMap__validateBitRange")
        so = mocker.patch.object(
            self.under_test, "_BitMap__validateSourceOverlaps"
        )
        self.under_test.mapBits(
            BitRange([0, 4]), BitRange([0, 4]), self.mock_field
        )

        assert self.mock_field in self.under_test.destinations

        self.under_test.removeDestination(self.mock_field)

        assert self.mock_field not in self.under_test.destinations
        check_removed_from_source_mapping()

        # Check that the Field (destination) has been reciprocally called for source removal.
        assert (
            self.mock_field.method_calls[-1][0] == "bitMap._removeReciprocalMap"
        )

    def test_remove_nonexistent_destination_interval_raises(self, this_context):
        # Removing a nonexistent destination interval raises an exception
        with pytest.raises(
            ConfigurationError,
            match="^Specified destination does not exist in bit map and cannot be removed",
        ):
            self.under_test.removeDestination(self.mock_field)

    def test_remove_destination_interval(self, this_context, mocker):
        # Removing a valid destination interval also removes the corresponding source interval.
        br = mocker.patch.object(self.under_test, "_BitMap__validateBitRange")
        so = mocker.patch.object(
            self.under_test, "_BitMap__validateSourceOverlaps"
        )
        self.under_test.mapBits(
            BitRange([0, 4]), BitRange([0, 4]), self.mock_field
        )
        self.under_test.mapBits(
            BitRange([5, 6]), BitRange([5, 6]), self.mock_field
        )

        assert self.mock_field in self.under_test.destinations

        self.under_test.removeDestination(
            destination=self.mock_field, interval=[0, 4]
        )

        # The actual objects must not have been removed.
        assert self.mock_field in self.under_test.destinations
        assert self.mock_register == self.under_test.source

        # But the interval must have been removed.
        self.check_removed_source_interval(BitRange([0, 4]))
        self.check_removed_destination_interval(BitRange([0, 4]))

        # Check that the Field (destination) has been reciprocally called for interval removal.
        assert (
            self.mock_field.method_calls[-1][0] == "bitMap._removeReciprocalMap"
        )

    def check_removed_source_interval(self, range):
        source_mappings = self.under_test.intervalMap.keys()
        assert range not in source_mappings

    def check_removed_destination_interval(self, range):
        destination_mappings = self.under_test.intervalMap.values()
        for k in destination_mappings:
            assert k["interval"] != range

    def test_remove_source_interval(self, this_context, mocker):
        # Removing a valid source interval also removes the corresponding destination interval.
        br = mocker.patch.object(self.under_test, "_BitMap__validateBitRange")
        so = mocker.patch.object(
            self.under_test, "_BitMap__validateSourceOverlaps"
        )
        self.under_test.mapBits(
            BitRange([0, 4]), BitRange([0, 4]), self.mock_field
        )
        self.under_test.mapBits(
            BitRange([5, 6]), BitRange([5, 6]), self.mock_field
        )

        assert self.mock_field in self.under_test.destinations

        self.under_test.removeSource(interval=[0, 4])

        # The actual objects must not have been removed.
        assert self.mock_field in self.under_test.destinations
        assert self.mock_register == self.under_test.source

        # But the interval must have been removed.
        self.check_removed_source_interval(BitRange([0, 4]))
        self.check_removed_destination_interval(BitRange([0, 4]))

        # Check that the Field (destination) has been reciprocally called for interval removal.
        assert (
            self.mock_field.method_calls[-1][0] == "bitMap._removeReciprocalMap"
        )

    def test_remove_source_raises(self, this_context):
        # Removing a source object raises an exception.
        with pytest.raises(
            ConfigurationError, match="^Cannot remove source from bit map"
        ):
            self.under_test.removeSource(self.mock_register)
