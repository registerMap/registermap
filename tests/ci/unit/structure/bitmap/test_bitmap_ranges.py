#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.bitmap.bitmap import (
    BitMap,
    BitRange,
    ConfigurationError,
)

from .mocks import MockBitStore


class TestBitMapAssignRanges:
    @pytest.fixture()
    def this_context(self):
        self.source_size = 16
        self.source_id = "testSource"
        self.mock_source = MockBitStore(self.source_id, self.source_size)
        self.source_bit_map = BitMap(self.mock_source)
        self.mock_source.bitMap = self.source_bit_map

        self.destination_size = 4
        self.destination_id = "testDestination"
        self.mock_destination = MockBitStore(
            self.destination_id, self.destination_size
        )
        self.destination_bit_map = BitMap(self.mock_destination)
        self.mock_destination.bitMap = self.destination_bit_map

    def test_map_bit_range(self, this_context):
        # Mapping two intervals of equal size generates no errors
        field_range = BitRange((0, (self.destination_size - 1)))
        register_bit_offset = 4
        register_range = BitRange(
            (
                register_bit_offset,
                (register_bit_offset + self.destination_size - 1),
            )
        )

        self.source_bit_map.mapBits(
            register_range, field_range, self.mock_destination
        )

    def test_assign_bad_source_bits_type_raises(self, this_context):
        # Specifying "not BitRange" for the source range raises.
        with pytest.raises(
            RuntimeError, match="^Incorrect bits type for source"
        ):
            self.source_bit_map.mapBits(
                5, BitRange((4, 6)), self.mock_destination
            )

    def test_source_bits_exceeds_source_size_raises(self, this_context):
        # Specifying register/source range that exceed the source size raises.
        field_range = BitRange((0, 5))
        register_range = BitRange((0, (self.source_size + 1)))
        with pytest.raises(
            RuntimeError, match="^Range source cannot exceed size"
        ):
            self.source_bit_map.mapBits(
                register_range, field_range, self.mock_destination
            )

    def test_destination_bits_exceeds_destination_size_raises(
        self, this_context
    ):
        # Specifying field/destination range that exceeds the destination size raises.
        destination_range = BitRange((0, (self.destination_size + 1)))
        source_range = BitRange((0, 5))
        with pytest.raises(
            RuntimeError, match="^Range destination cannot exceed size"
        ):
            self.source_bit_map.mapBits(
                source_range, destination_range, self.mock_destination
            )

    def test_assign_bad_destination_bits_type_raises(self, this_context):
        # Specifying "not BitRange" type for destination range raises.
        with pytest.raises(
            RuntimeError, match="^Incorrect bits type for destination"
        ):
            self.source_bit_map.mapBits(
                BitRange((4, 6)), 8, self.mock_destination
            )

    def test_assign_mismatched_sizes(self, this_context):
        # Specifying source and destination ranges that don't have the same size raises.
        destination_range = BitRange((0, (self.destination_size - 1)))
        source_range = BitRange((4, 10))

        with pytest.raises(
            ConfigurationError, match="^Mismatched bit range sizes"
        ):
            self.source_bit_map.mapBits(
                source_range, destination_range, self.mock_destination
            )
