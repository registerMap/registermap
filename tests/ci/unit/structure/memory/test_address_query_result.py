#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.memory.address import AddressQueryResult


class TestAddressQueryResult:
    @pytest.fixture()
    def this_context(self):
        self.under_test = AddressQueryResult()

    def test_default(self, this_context):
        assert self.under_test.address is None

        assert self.under_test.module is None
        assert self.under_test.register is None

        # fields must be an empty list
        assert not self.under_test.fields
