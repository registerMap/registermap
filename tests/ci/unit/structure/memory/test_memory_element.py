#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import logging

import pytest

from registermap.structure.memory.element import (
    MemoryUnitMemoryElement,
    SizeValue,
)

log = logging.getLogger(__name__)


class TestMemoryUnitsMemoryElementDefaultConstructor:
    @pytest.fixture()
    def this_context(self):
        self.memory_unit_bits = SizeValue(8)

    def test_default_arguments(self, this_context):
        under_test = MemoryUnitMemoryElement(self.memory_unit_bits)

        assert under_test.sizeBits is None
        assert under_test.sizeMemoryUnits is None

    def test_assign_size_bits(self, this_context):
        under_test = MemoryUnitMemoryElement(self.memory_unit_bits)

        under_test.sizeBits = 16

        assert 16 == under_test.sizeBits
        assert 2 == under_test.sizeMemoryUnits

    def test_assign_size_memory_units(self, this_context):
        under_test = MemoryUnitMemoryElement(self.memory_unit_bits)

        under_test.sizeMemoryUnits = 4

        assert 32 == under_test.sizeBits
        assert 4 == under_test.sizeMemoryUnits

    def test_assign_size_bits_raises(self, this_context):
        """Assigning a non integer multiple of memory_unit_bits raises an exception."""
        under_test = MemoryUnitMemoryElement(self.memory_unit_bits)

        with pytest.raises(
            RuntimeError,
            match="^Cannot assign bits a non integer multiple of memory units",
        ):
            under_test.sizeBits = 15
