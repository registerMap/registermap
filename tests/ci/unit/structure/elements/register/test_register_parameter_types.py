#
# Copyright 2017 Russell Smiley
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import collections

import pytest

from registermap.exceptions import ConfigurationError
from registermap.structure.elements.register.register import (
    BitFieldsParameter,
    Field,
    ModeParameter,
    PublicParameter,
)


class TestBitFieldsParameter:
    def test_initialization(self):
        p = BitFieldsParameter()

        assert p.name == "bitFields"
        assert isinstance(p.value, collections.OrderedDict)

    def test_empty_bit_fields_to_yaml_data(self):
        p = BitFieldsParameter()

        expected_yaml_data = {"bitFields": list()}
        actual_yaml_data = p.to_yamlData()

        assert actual_yaml_data == expected_yaml_data

    def test_single_bit_field_to_yaml_data(self):
        p = BitFieldsParameter()
        p.value["f1"] = self.create_bit_field("f1", 3)

        expected_yaml_data = {"bitFields": [p.value["f1"].to_yamlData()]}
        actual_yaml_data = p.to_yamlData()

        assert actual_yaml_data == expected_yaml_data

    def test_multiple_bit_field_to_yaml_data(self):
        p = BitFieldsParameter()
        p.value["f1"] = self.create_bit_field("f1", 3)
        p.value["f2"] = self.create_bit_field("f2", 1)

        expected_yaml_data = {
            "bitFields": [
                p.value["f1"].to_yamlData(),
                p.value["f2"].to_yamlData(),
            ]
        }
        actual_yaml_data = p.to_yamlData()

        assert actual_yaml_data == expected_yaml_data

    def create_bit_field(self, name, size):
        b = Field()
        b["name"] = name
        b["size"] = size

        return b

    def test_from_good_yaml_data(self):
        p = BitFieldsParameter()
        p.value["f1"] = self.create_bit_field("f1", 3)
        p.value["f2"] = self.create_bit_field("f2", 1)

        yaml_data = p.to_yamlData()
        gp = BitFieldsParameter.from_yamlData(yaml_data)

        assert gp.value["f1"]["name"] == "f1"
        assert gp.value["f1"]["size"] == 3
        assert gp.value["f2"]["name"] == "f2"
        assert gp.value["f2"]["size"] == 1

    def test_optional_yaml_data(self):
        yaml_data = {"mode": "ro"}
        gp = BitFieldsParameter.from_yamlData(yaml_data, optional=True)


class TestModeParameter:
    def test_initialization(self):
        expected_name = "mode"
        expected_value = "rw"
        p = ModeParameter(expected_value)

        assert p.name == expected_name
        assert p.value == expected_value

    def test_initialization_bad_value_raises(self):
        bad_value = "badValue"
        with pytest.raises(ConfigurationError, match="^Invalid value"):
            ModeParameter(bad_value)

    def test_good_value_initialization_no_raise(self):
        for value in ModeParameter.validModes:
            ModeParameter(value)

    def test_validate_bad_value_raises(self):
        p = ModeParameter("rw")

        bad_value = "badValue"
        with pytest.raises(ConfigurationError, match="^Invalid value"):
            p.validate(bad_value)

    def test_to_yaml_data(self):
        expected_value = "ro"
        p = ModeParameter(expected_value)

        expected_yaml_data = {"mode": expected_value}
        actual_yaml_data = p.to_yamlData()

        assert actual_yaml_data == expected_yaml_data

    def test_from_good_yaml_data(self):
        expected_value = "ro"
        yaml_data = {"mode": expected_value}

        p = ModeParameter.from_yamlData(yaml_data)

        assert p.name == "mode"
        assert p.value == expected_value

    def test_bad_yaml_data_raises(self):
        yaml_data = {"mode": "badvalue"}

        with pytest.raises(ConfigurationError, match="^Invalid value"):
            ModeParameter.from_yamlData(yaml_data)

    def test_optional_yaml_data(self):
        yaml_data = {"public": True}

        p = ModeParameter.from_yamlData(yaml_data, optional=True)

        assert p.name == "mode"
        assert p.value == "rw"


class TestPublicParameter:
    def test_initialization(self):
        expected_name = "public"
        expected_value = True
        p = PublicParameter(expected_value)

        assert p.name == expected_name
        assert p.value == expected_value

    def test_initialization_bad_value_raises(self):
        bad_value = 1
        with pytest.raises(
            ConfigurationError, match="^Public must be specified as boolean"
        ):
            PublicParameter(bad_value)

    def test_validate_bad_value_raises(self):
        p = PublicParameter(False)

        bad_value = 1
        with pytest.raises(
            ConfigurationError, match="^Public must be specified as boolean"
        ):
            p.validate(bad_value)

    def test_to_yaml_data(self):
        expected_value = True
        p = PublicParameter(expected_value)

        expected_yaml_data = {"public": expected_value}
        actual_yaml_data = p.to_yamlData()

        assert actual_yaml_data == expected_yaml_data

    def test_from_good_yaml_data(self):
        expected_values = [True, False]

        for expected_value in expected_values:
            yaml_data = {"public": expected_value}

            p = PublicParameter.from_yamlData(yaml_data)

            assert p.name == "public"
            assert p.value == expected_value

    def test_bad_yaml_data_raises(self):
        yaml_data = {"public": "true"}

        with pytest.raises(
            ConfigurationError, match="^Public must be specified as boolean"
        ):
            PublicParameter.from_yamlData(yaml_data)

    def test_optional_yaml_data(self):
        expected_value = True

        yaml_data = {"other": "somevalue"}

        p = PublicParameter.from_yamlData(yaml_data, optional=True)

        assert p.name == "public"
        assert p.value == expected_value
