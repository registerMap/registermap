#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import logging

import pytest

from registermap.exceptions import ConstraintError
from registermap.structure.elements.module import Module
from registermap.structure.elements.register.instance import RegisterInstance
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set import SetCollection

from ..mock_observer import MockObserver
from .mocks import MockPreviousRegister

log = logging.getLogger(__name__)


class TestRegisterAddressPreviousNoneAddress:
    """
    Test register address when the previous address has no defined address or size.
    """

    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.previous_register = MockPreviousRegister()
        self.test_bit_field_set = SetCollection()
        self.test_space = MemoryConfiguration()
        self.register_under_test = RegisterInstance(
            self.test_space, setCollection=self.test_bit_field_set
        )

        self.register_under_test.previousElement = self.previous_register

        self.register_under_test.sizeChangeNotifier.addObserver(self.observer)
        self.register_under_test.addressChangeNotifier.addObserver(
            self.observer
        )

    def test_default_value(self, this_context):
        assert self.register_under_test.startAddress is None
        assert self.register_under_test.endAddress is None

    def test_fixed_address(self, this_context):
        expected_value = 0x15

        assert self.register_under_test.startAddress != expected_value

        self.register_under_test["constraints"]["fixedAddress"] = expected_value

        assert expected_value == self.register_under_test.startAddress
        assert expected_value == self.register_under_test.endAddress
        assert self.observer.update_count == 1

    def test_aligned_address(self, this_context):
        alignment_value = 2

        self.register_under_test["constraints"][
            "alignmentMemoryUnits"
        ] = alignment_value

        # Attempt to constrain alignment with no concrete addresses does nothing
        assert self.register_under_test.startAddress is None
        assert self.register_under_test.endAddress is None
        assert self.observer.update_count == 0


class TestRegisterAddressPreviousConcreteAddress:
    """
    Test register address when the previous register has concrete address and size.
    """

    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.previous_register = MockPreviousRegister(
            startAddress=0x10, sizeMemoryUnits=5
        )
        self.test_bit_field_set = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = RegisterInstance(
            self.test_space, setCollection=self.test_bit_field_set
        )

        self.under_test.previousElement = self.previous_register

        self.under_test.sizeChangeNotifier.addObserver(self.observer)
        self.under_test.addressChangeNotifier.addObserver(self.observer)

    def test_fixed_address(self, this_context):
        expected_value = 0x16

        assert 1 == self.under_test.sizeMemoryUnits
        assert self.under_test.startAddress != expected_value

        self.under_test["constraints"]["fixedAddress"] = expected_value

        assert expected_value == self.under_test.startAddress
        assert expected_value == self.under_test.endAddress
        assert self.observer.update_count == 1

    def test_fixed_address_on_previous_raises(self, this_context):
        expected_value = self.previous_register.endAddress

        with pytest.raises(ConstraintError, match="^Fixed address exceeded"):
            self.under_test["constraints"]["fixedAddress"] = expected_value

    def test_aligned_address(self, this_context):
        alignment_value = 2
        expected_value = self.previous_register.endAddress + 2

        assert 1 == self.under_test.sizeMemoryUnits
        assert 0 == (expected_value % alignment_value)
        assert self.under_test.startAddress < expected_value

        self.under_test["constraints"]["alignmentMemoryUnits"] = alignment_value

        assert expected_value == self.under_test.startAddress
        assert expected_value == self.under_test.endAddress
        assert self.observer.update_count == 1

    def test_end_address_multiple_memory_units(self, this_context):
        f1 = self.under_test.addField("f1", (0, 10))

        assert 1 < self.under_test.sizeMemoryUnits

        start_address = self.under_test.startAddress

        expected_end_address = (
            start_address + self.under_test.sizeMemoryUnits - 1
        )

        assert expected_end_address == self.under_test.endAddress

    def test_end_address_multiple_fields_same_size(self, this_context):
        def check_expected_end_address():
            start_address = self.under_test.startAddress

            expected_end_address = (
                start_address + self.under_test.sizeMemoryUnits - 1
            )

            assert expected_end_address == self.under_test.endAddress

        self.under_test.addField("f1", (0, 3))
        assert 1 == self.under_test.sizeMemoryUnits

        check_expected_end_address()

        # Add a second field that doesn't change the size of the register.
        self.under_test.addField("f2", (5, 7))
        assert 1 == self.under_test.sizeMemoryUnits

        check_expected_end_address()

    def test_end_address_multiple_memory_units_multiple_fields(
        self, this_context
    ):
        def check_expected_end_address():
            start_address = self.under_test.startAddress

            expected_end_address = (
                start_address + self.under_test.sizeMemoryUnits - 1
            )

            assert expected_end_address == self.under_test.endAddress

        self.under_test.addField("f1", (0, 5))
        assert 1 == self.under_test.sizeMemoryUnits

        check_expected_end_address()

        # Add an adjacent field that extends to a second memory unit.
        self.under_test.addField("f2", (6, 10))
        assert 2 == self.under_test.sizeMemoryUnits

        check_expected_end_address()

        # Add a field with a gap that extends to a third memory unit.
        self.under_test.addField("f3", (15, 18))
        assert 3 == self.under_test.sizeMemoryUnits

        check_expected_end_address()


class TestRegisterPageRegisterInteraction:
    @pytest.fixture()
    def this_context(self):
        self.test_bit_field_set = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = RegisterInstance(
            self.test_space, setCollection=self.test_bit_field_set
        )

        self.previous_register = MockPreviousRegister(
            startAddress=0, sizeMemoryUnits=4
        )
        self.under_test.previousElement = self.previous_register

    def test_page_size(self, this_context):
        assert 32 == self.test_space.addressBits
        assert 8 == self.test_space.memoryUnitBits

        self.previous_register.address = 0x278

        log.debug(
            "Mock previous register start address: "
            + hex(self.previous_register.address)
        )
        log.debug(
            "Mock previous register end address: "
            + hex(self.previous_register.endAddress)
        )
        assert 0x278 == self.previous_register.address
        log.debug(
            "Test register start address no page size: "
            + hex(self.under_test.startAddress)
        )
        log.debug(
            "Test register end address no page size: "
            + hex(self.under_test.endAddress)
        )
        assert 0x27C == self.under_test.startAddress

        self.test_space.pageSize = 0x80
        log.debug(
            "Test register start address page size {0}: {1}".format(
                hex(self.test_space.pageSize), hex(self.under_test.startAddress)
            )
        )
        log.debug(
            "Test register end address page size {0}: {1}".format(
                hex(self.test_space.pageSize), hex(self.under_test.endAddress)
            )
        )
        assert 0x280 == self.under_test.startAddress


class TestRegisterPreviousRegister:
    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.test_bit_field_set = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = RegisterInstance(
            self.test_space, setCollection=self.test_bit_field_set
        )

        self.under_test.sizeChangeNotifier.addObserver(self.observer)

    def test_default_value(self, this_context):
        assert self.under_test.previousElement is None

    def test_previous_register_assign(self, this_context):
        expected_value = 0x10
        self.under_test.previousElement = MockPreviousRegister(
            startAddress=0x5, endAddress=(expected_value - 1)
        )

        assert expected_value == self.under_test.startAddress

    def test_unassigned_previous_register_none_address(self, this_context):
        fixed_address = 0x10
        self.under_test["constraints"]["fixedAddress"] = fixed_address

        assert fixed_address == self.under_test.startAddress

    def test_unassigned_end_address(self, this_context):
        self.under_test.previousElement = MockPreviousRegister()

        actualAddress = self.under_test.startAddress
        assert actualAddress is None

    def test_assign_previous_register_end_address(self, this_context):
        self.under_test.previousElement = MockPreviousRegister(startAddress=0x5)

        assert self.under_test.startAddress is None

        expected_address = 0x10
        self.under_test.previousElement.endAddress = expected_address - 1

        actual_address = self.under_test.startAddress
        assert expected_address == actual_address


class TestRegisterOffset:
    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.previous_register = MockPreviousRegister()
        self.test_bit_field_set = SetCollection()
        self.test_space = MemoryConfiguration()
        self.test_space.baseAddress = 0x20F0

        self.under_test = RegisterInstance(
            self.test_space, setCollection=self.test_bit_field_set
        )

        self.under_test.previousElement = self.previous_register

        self.under_test.sizeChangeNotifier.addObserver(self.observer)
        self.under_test.addressChangeNotifier.addObserver(self.observer)

    def test_default_value(self, this_context):
        assert self.under_test.offset is None

    def test_data_assignment(self, this_context):
        expected_value = 0xDE
        input_value = self.test_space.baseAddress + expected_value

        assert expected_value != self.under_test.offset

        self.under_test["constraints"]["fixedAddress"] = input_value

        assert input_value == self.under_test.startAddress

        assert expected_value == self.under_test.offset


class TestRegisterModuleOffset:
    @pytest.fixture()
    def this_context(self, mocker):
        self.observer = MockObserver()
        self.previous_register = MockPreviousRegister()
        self.test_bit_field_set = SetCollection()
        self.test_space = MemoryConfiguration()
        self.test_space.baseAddress = 0x20F0
        self.mock_module = mocker.create_autospec(Module)
        self.mock_module.baseAddress = 0x21F0

        self.under_test = RegisterInstance(
            self.test_space,
            parent=self.mock_module,
            setCollection=self.test_bit_field_set,
        )

        self.under_test.previousElement = self.previous_register

        self.under_test.sizeChangeNotifier.addObserver(self.observer)
        self.under_test.addressChangeNotifier.addObserver(self.observer)

    def test_default_value(self, this_context):
        assert self.under_test.offset is None

    def test_data_assignment(self, this_context):
        expected_value = 0xDE
        input_value = self.mock_module.baseAddress + expected_value

        assert expected_value != self.under_test.moduleOffset

        self.under_test["constraints"]["fixedAddress"] = input_value

        assert input_value == self.under_test.startAddress

        assert expected_value == self.under_test.moduleOffset
