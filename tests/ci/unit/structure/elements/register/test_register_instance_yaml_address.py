#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.elements.register.instance import RegisterInstance
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set import SetCollection

from ..mock_observer import MockObserver
from .mocks import MockPreviousRegister


class TestRegisterInstanceYamlAddress:
    @pytest.fixture()
    def this_context(self):
        self.set_collection = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = RegisterInstance(
            self.test_space, setCollection=self.set_collection
        )

        self.previous_register = MockPreviousRegister(
            endAddress=0x3E7, sizeMemoryUnits=4
        )
        self.under_test.previousElement = self.previous_register

        self.observer = MockObserver()
        self.under_test.sizeChangeNotifier.addObserver(self.observer)

    def test_yaml_data_address_single_register(self, this_context):
        # The address data is automatically generated so it is prefixed by '_'.
        assert self.previous_register.endAddress == 0x3E7

        expected_name = "_address"
        expected_value = 0x3E8

        assert expected_value == self.under_test.startAddress

        yaml_data = self.under_test.to_yamlData()
        assert expected_value == yaml_data["register"][expected_name]
