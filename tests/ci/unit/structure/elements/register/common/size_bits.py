#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.exceptions import ConstraintError
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set import SetCollection

from ...mock_observer import MockObserver


class CommonSizeBitsTests:
    class TestRegisterSizeBits:
        # The tests will fail unless a test case loader correctly fulfills this value.
        RegisterType = None

        @pytest.fixture()
        def this_context(self):
            self.observer = MockObserver()
            self.set_collection = SetCollection()
            self.test_space = MemoryConfiguration()
            self.under_test = self.RegisterType(
                self.test_space, setCollection=self.set_collection
            )

            self.under_test.sizeChangeNotifier.addObserver(self.observer)

        def test_correct_size_for_fields_added(self, this_context):
            assert 0 == self.observer.update_count

            memory_units_size_bits = self.test_space.memoryUnitBits

            self.under_test.addField("f1", [0, 7], (0, 7))
            assert memory_units_size_bits == self.under_test.sizeBits

            self.under_test.addField("f2", [8, 10], (0, 2))
            assert (2 * memory_units_size_bits) == self.under_test.sizeBits

    class TestRegisterSize:
        # The tests will fail unless a test case loader correctly fulfills this value.
        RegisterType = None

        @pytest.fixture()
        def this_context(self):
            self.observer = MockObserver()
            self.set_collection = SetCollection()
            self.test_space = MemoryConfiguration()
            self.under_test = self.RegisterType(
                self.test_space, setCollection=self.set_collection
            )

            self.under_test.sizeChangeNotifier.addObserver(self.observer)

        def test_default_value(self, this_context):
            # A register with no bit fields must allocate one memory unit for itself.
            assert 1 == self.under_test.sizeMemoryUnits

        def test_correct_size_for_bit_fields_added1(self, this_context):
            # Adding two intervals to a register increases the register size:
            #  - from different fields
            #  - the second register interval exceeds the existing register size
            assert 8 == self.test_space.memoryUnitBits
            assert 0 == self.observer.update_count

            self.under_test.addField("f1", [0, 7], (0, 7))
            assert 1 == self.under_test.sizeMemoryUnits
            # No notifications because the field addition didn't change the register size.
            assert 0 == self.observer.update_count

            self.under_test.addField("f2", [8, 10], (0, 2))
            assert 2 == self.under_test.sizeMemoryUnits
            assert 1 == self.observer.update_count
            assert 8 == self.under_test["fields"]["f1"]["size"]
            assert 3 == self.under_test["fields"]["f2"]["size"]

        def test_correct_size_for_bit_fields_added2(self, this_context):
            # Adding two intervals to a register increases the register size:
            #  - from different fields
            #  - the first register interval does not exceed the register size, but the second interval does
            assert 8 == self.test_space.memoryUnitBits
            assert 0 == self.observer.update_count

            self.under_test.addField("f1", [0, 3], (0, 3))
            assert 1 == self.under_test.sizeMemoryUnits
            assert 0 == self.observer.update_count

            self.under_test.addField("f2", [8, 10], (0, 2))
            assert 2 == self.under_test.sizeMemoryUnits
            assert 1 == self.observer.update_count
            assert 4 == self.under_test["fields"]["f1"]["size"]
            assert 3 == self.under_test["fields"]["f2"]["size"]

        def test_correct_size_for_bit_fields_added3(self, this_context):
            # Adding two intervals to a register does not increase the register size:
            #  - from the same field
            #  - the extent of the new register intervals is less than the size of the register
            #  - the extent of the second field interval changes the size of the field
            #  - the FieldSet does not change size
            assert 8 == self.test_space.memoryUnitBits
            assert 0 == self.observer.update_count

            self.under_test.addField("f1", [0, 3], (0, 3))
            assert 1 == self.under_test.sizeMemoryUnits
            assert 4 == self.under_test["fields"]["f1"]["size"]
            # No notifications because the field addition didn't change the register size.
            assert 0 == self.observer.update_count
            self.under_test.addField("f1", [6, 7], (5, 6))

            assert 1 == self.under_test.sizeMemoryUnits
            assert 0 == self.observer.update_count
            assert 7 == self.under_test["fields"]["f1"]["size"]
            assert 1 == len(self.set_collection.fieldSet)
            assert 2 == len(self.under_test.bitMap.sourceIntervals)

        def test_fixed_size_exceeded_add_bitfield_raises(self, this_context):
            assert 8 == self.test_space.memoryUnitBits

            self.under_test["constraints"]["fixedSizeMemoryUnits"] = 1
            self.under_test.addField("f1", [0, 6], [0, 6])
            with pytest.raises(ConstraintError, match="^Fixed size exceeded"):
                self.under_test.addField("f2", [8, 10], (0, 2))

        def test_fixed_size_constraint_reports_as_size(self, this_context):
            assert 8 == self.test_space.memoryUnitBits

            expected_size = 2
            update_count_before_add_field = self.observer.update_count

            self.under_test["constraints"][
                "fixedSizeMemoryUnits"
            ] = expected_size

            assert self.under_test.sizeMemoryUnits == expected_size
            assert (
                update_count_before_add_field + 1
            ) == self.observer.update_count

        def test_single_field_spans_multiple_bytes(self, this_context):
            assert 8 == self.test_space.memoryUnitBits

            expected_size = 2
            update_count_before_add_field = self.observer.update_count

            self.under_test.addField("f1", [0, 10])

            assert self.under_test.sizeMemoryUnits == expected_size
            assert (
                update_count_before_add_field + 1
            ) == self.observer.update_count
