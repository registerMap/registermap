#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set import SetCollection


class CommonFieldIntervalTests:
    class TestRegisterDefaultFieldInterval:
        # The tests will fail unless a test case loader correctly fulfills this value.
        RegisterType = None

        @pytest.fixture()
        def this_context(self):
            self.set_collection = SetCollection()
            self.memory_space = MemoryConfiguration()

            self.register_under_test = self.RegisterType(
                self.memory_space, setCollection=self.set_collection
            )

        def test_default_field_interval(self, this_context):
            # When creating a new field, only specifying the register interval assume the field is the size of the register.
            assert len(self.set_collection.fieldSet) == 0

            self.register_under_test.addField("new_field", [5, 8])

            new_fields = self.set_collection.fieldSet.find("new_field")
            assert len(new_fields) == 1

            new_field = new_fields.pop()
            assert new_field.sizeBits == 4

        def test_missing_field_interval_asserts(self, this_context):
            # Not specifying the field interval when changing an existing field asserts

            self.register_under_test.addField("newField", [5, 8])
            assert len(self.set_collection.fieldSet) == 1

            with pytest.raises(AssertionError):
                self.register_under_test.addField("newField", [9, 10])
