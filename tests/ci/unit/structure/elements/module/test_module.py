"""
Unit test Module class
"""
#
# Copyright 2016 Russell Smiley
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import logging

import pytest

from registermap.exceptions import ConstraintError
from registermap.structure.elements.module.module import (
    Module,
    RegistersParameter,
)
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set import SetCollection

from ..mock_observer import MockObserver
from .common import CommonModuleInterfaceTests
from .mocks import MockPreviousModule

log = logging.getLogger(__name__)


class TestFirstRegister:
    @pytest.fixture()
    def this_context(self):
        pass

    def test_get_end_address_property(self, this_context):
        expected_value = 0x10
        first_register = RegistersParameter.FirstRegister(
            endAddress=expected_value
        )
        assert expected_value == first_register.endAddress

    def test_set_end_address_property(self, this_context):
        expected_value = 0x10
        first_register = RegistersParameter.FirstRegister(endAddress=0x20)
        assert first_register.endAddress != expected_value

        first_register.endAddress = expected_value

        assert expected_value == first_register.endAddress


class TestModule:
    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.set_collection = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = Module(self.test_space, self.set_collection)
        self.under_test["name"] = "module"

        self.under_test.sizeChangeNotifier.addObserver(self.observer)
        self.under_test.addressChangeNotifier.addObserver(self.observer)

    def test_default_memory(self, this_context):
        assert 8 == self.under_test.memory.memoryUnitBits

        assert self.under_test.baseAddress is None
        assert self.under_test.endAddress is None
        assert self.under_test.previousElement is None
        assert 0 == self.under_test.spanMemoryUnits
        assert 0 == self.under_test.assignedMemoryUnits

    def test_add_single_register(self, this_context):
        assert 8 == self.under_test.memory.memoryUnitBits

        r = self.under_test.addRegister("r1")

        assert self.under_test.baseAddress is None
        assert self.under_test.endAddress is None
        assert self.under_test.previousElement is None
        assert 0 == self.under_test.spanMemoryUnits
        assert 1 == self.under_test.assignedMemoryUnits
        assert "module.r1" == r.canonicalId

    def test_review_address_change_empty_module(self, this_context):
        under_test = Module(self.test_space, self.set_collection)

        assert 0 == len(under_test["registers"])

        # No exceptions should be thrown
        under_test.reviewAddressChange()

        assert under_test.baseAddress is None

    def test_review_address_change_empty_module_from_yaml(self, this_context):
        under_test = Module(self.test_space, self.set_collection)

        assert 0 == len(under_test["registers"])

        yaml_data = under_test.to_yamlData()
        generated_module = Module.from_yamlData(
            yaml_data, self.test_space, self.set_collection
        )

        # No exceptions should be thrown
        generated_module.reviewAddressChange()

        assert generated_module.baseAddress is None


class TestModuleWithPreviousModule:
    @pytest.fixture()
    def this_context(self):
        self.previous_module = MockPreviousModule(endAddress=0x10)
        self.observer = MockObserver()
        self.set_collection = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = Module(self.test_space, self.set_collection)

        self.under_test.previousElement = self.previous_module

        self.under_test.sizeChangeNotifier.addObserver(self.observer)
        self.under_test.addressChangeNotifier.addObserver(self.observer)

    def test_default_memory(self, this_context):
        assert 8 == self.under_test.memory.memoryUnitBits

        assert (
            self.previous_module.endAddress + 1
        ) == self.under_test.baseAddress
        # size 0 implies the end address of the module must be "before" the base address.
        assert (self.under_test.baseAddress - 1) == self.under_test.endAddress
        assert 0 == self.under_test.spanMemoryUnits
        assert 0 == self.under_test.assignedMemoryUnits

    def test_add_single_register(self, this_context):
        assert 8 == self.under_test.memory.memoryUnitBits

        self.under_test.addRegister("r1")

        assert (
            self.previous_module.endAddress + 1
        ) == self.under_test.baseAddress
        # size 1 implies the end address of the module must be the same as the base address.
        assert self.under_test.baseAddress == self.under_test.endAddress
        assert 1 == self.under_test.spanMemoryUnits
        assert 1 == self.under_test.assignedMemoryUnits


class TestModuleConstraints(CommonModuleInterfaceTests.TestModuleConstraints):
    def construct_instance_under_test(self):
        under_test = Module(self.mock_memory, self.mock_setCollection)
        self.mock_observer = MockObserver()

        under_test.sizeChangeNotifier.addObserver(self.mock_observer)
        under_test.addressChangeNotifier.addObserver(self.mock_observer)

        return under_test

    def test_add_register_over_fixed_size_raises(self, this_context):
        assert 0 == self.under_test.spanMemoryUnits
        assert 0 == self.under_test.assignedMemoryUnits
        assert 8 == self.mock_memory.memoryUnitBits

        self.mock_previousElement.endAddress = 0x10

        self.under_test["constraints"]["fixedSizeMemoryUnits"] = 3

        r1 = self.under_test.addRegister("r1")
        r1.addField("f1", [0, 10], (0, 10))
        self.under_test.addRegister("r2")

        assert 3 == self.under_test.spanMemoryUnits

        with pytest.raises(ConstraintError, match="^Fixed size exceeded"):
            # A register has a size of one memory unit even if it has no bit fields.
            # So adding a third register must exceed the size limit
            self.under_test.addRegister("r3")


class TestModuleDescription:
    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.set_collection = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = Module(self.test_space, self.set_collection)

        self.under_test.sizeChangeNotifier.addObserver(self.observer)
        self.under_test.addressChangeNotifier.addObserver(self.observer)

    def test_default_value(self, this_context):
        assert "" == self.under_test["description"]


class TestModuleNameParameter(
    CommonModuleInterfaceTests.TestModuleNameParameter
):
    def construct_instance_under_test(self):
        under_test = Module(self.mock_memory, self.set_collection)

        self.observer = MockObserver()
        under_test.sizeChangeNotifier.addObserver(self.observer)
        under_test.addressChangeNotifier.addObserver(self.observer)

        return under_test


class TestModuleOffsetProperty(
    CommonModuleInterfaceTests.TestModuleOffsetProperty
):
    def construct_instance_under_test(self):
        under_test = Module(self.mock_memory, self.set_collection)

        under_test.addRegister("r1")

        return under_test


class TestModulePageRegisterInteraction:
    @pytest.fixture()
    def this_context(self):
        self.set_collection = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = Module(self.test_space, self.set_collection)

        self.previous_module = MockPreviousModule(endAddress=0)
        self.under_test.previousElement = self.previous_module

    def test_module_on_page_register(self, this_context):
        assert 32 == self.test_space.addressBits
        assert 8 == self.test_space.memoryUnitBits

        self.previous_module.endAddress = 0x27B

        log.debug(
            "Mock previous module end address: "
            + hex(self.previous_module.endAddress)
        )
        assert 0x27B == self.previous_module.endAddress
        log.debug(
            "Test module start address no page size: "
            + hex(self.under_test.baseAddress)
        )
        log.debug(
            "Test module end address no page size: "
            + hex(self.under_test.endAddress)
        )
        assert 0x27C == self.under_test.baseAddress

        self.test_space.pageSize = 0x80
        log.debug(
            "Test module start address page size {0}: {1}".format(
                hex(self.test_space.pageSize), hex(self.under_test.baseAddress)
            )
        )
        log.debug(
            "Test module end address page size {0}: {1}".format(
                hex(self.test_space.pageSize), hex(self.under_test.endAddress)
            )
        )
        assert 0x280 == self.under_test.baseAddress


class TestModulePreviousModule(
    CommonModuleInterfaceTests.TestModulePreviousElementProperty
):
    def construct_instance_under_test(self):
        under_test = Module(self.mock_memory, self.set_collection)

        self.observer = MockObserver()
        under_test.sizeChangeNotifier.addObserver(self.observer)
        under_test.addressChangeNotifier.addObserver(self.observer)

        return under_test


class TestModuleSizePreviousConcreteAddresses:
    # Module size is the number of memory units spanned by registers from the lowest memory unit to the highest
    # memory unit.
    # - If a module has a fixed address then the lowest memory unit is always the fixed address.
    # - If a page size is specified, then register addresses must miss paging registers (a fixed address on a page
    # register must raise).
    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.previous_module = MockPreviousModule(endAddress=0x10)
        self.set_collection = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = Module(self.test_space, self.set_collection)

        self.under_test.previousElement = self.previous_module

        self.under_test.sizeChangeNotifier.addObserver(self.observer)
        self.under_test.addressChangeNotifier.addObserver(self.observer)

    def test_default_value(self, this_context):
        assert 0 == self.under_test.spanMemoryUnits
        assert 0 == self.under_test.assignedMemoryUnits

    def test_contiguous_registers(self, this_context):
        assert 0 == self.under_test.spanMemoryUnits
        assert 0 == self.under_test.assignedMemoryUnits
        assert 8 == self.test_space.memoryUnitBits

        r1 = self.under_test.addRegister("r1")
        assert 1 == self.under_test.spanMemoryUnits
        assert 1 == self.under_test.assignedMemoryUnits

        r1.addField("f1", [0, 10], [0, 10])
        assert 2 == self.under_test.spanMemoryUnits
        assert 2 == self.under_test.assignedMemoryUnits

        r2 = self.under_test.addRegister("r2")

        assert 3 == self.under_test.spanMemoryUnits
        assert 3 == self.under_test.assignedMemoryUnits

    def test_discontiguous_registers(self, this_context):
        assert 0 == self.under_test.spanMemoryUnits
        assert 0 == self.under_test.assignedMemoryUnits
        assert 8 == self.test_space.memoryUnitBits
        assert (
            self.previous_module.endAddress + 1
        ) == self.under_test.baseAddress

        r1 = self.under_test.addRegister("r1")
        r1.addField("f1", [0, 10], (0, 10))
        assert 2 == self.under_test.spanMemoryUnits
        assert 2 == self.under_test.assignedMemoryUnits

        r2 = self.under_test.addRegister("r2")
        assert 1 == r2.sizeMemoryUnits
        assert 3 == self.under_test.spanMemoryUnits
        assert 3 == self.under_test.assignedMemoryUnits

        expected_address = 0x15
        r2["constraints"]["fixedAddress"] = expected_address

        assert r2.startAddress == expected_address
        assert (
            expected_address - self.under_test.baseAddress + 1
        ) == self.under_test.spanMemoryUnits
        assert 3 == self.under_test.assignedMemoryUnits

    def test_discontiguous_registers_with_multiunit_register(
        self, this_context
    ):
        assert 0 == self.under_test.spanMemoryUnits
        assert 0 == self.under_test.assignedMemoryUnits
        assert 8 == self.test_space.memoryUnitBits
        assert (
            self.previous_module.endAddress + 1
        ) == self.under_test.baseAddress

        r1 = self.under_test.addRegister("r1")
        assert 1 == self.under_test.spanMemoryUnits

        r2 = self.under_test.addRegister("r2")
        r2.addField("f1", [0, 10], (0, 10))
        assert 2 == r2.sizeMemoryUnits
        assert 3 == self.under_test.spanMemoryUnits
        assert 3 == self.under_test.assignedMemoryUnits

        expected_address = 0x15
        r2["constraints"]["fixedAddress"] = expected_address

        assert expected_address == r2.startAddress
        assert 6 == self.under_test.spanMemoryUnits
        assert 3 == self.under_test.assignedMemoryUnits


class TestModuleSizePreviousNoneAddresses:
    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.previous_module = MockPreviousModule(endAddress=None)
        self.set_collection = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = Module(self.test_space, self.set_collection)

        self.under_test.previousElement = self.previous_module

        self.under_test.sizeChangeNotifier.addObserver(self.observer)
        self.under_test.addressChangeNotifier.addObserver(self.observer)

    def test_discontiguous_registers(self, this_context):
        assert 0 == self.under_test.spanMemoryUnits
        assert 0 == self.under_test.assignedMemoryUnits
        assert 8 == self.test_space.memoryUnitBits
        assert self.under_test.baseAddress is None

        r1 = self.under_test.addRegister("r1")
        r1.addField("f1", [0, 10], (0, 10))
        assert 0 == self.under_test.spanMemoryUnits
        assert 2 == self.under_test.assignedMemoryUnits

        r2 = self.under_test.addRegister("r2")
        assert 1 == r2.sizeMemoryUnits
        assert 0 == self.under_test.spanMemoryUnits
        assert 3 == self.under_test.assignedMemoryUnits

        expected_address = 0x15
        r2["constraints"]["fixedAddress"] = expected_address

        assert r2.startAddress == expected_address
        assert 0 == self.under_test.spanMemoryUnits
        assert 3 == self.under_test.assignedMemoryUnits


class TestModuleSpanPreviousAddressChange:
    @pytest.fixture()
    def this_context(self):
        self.previous_module = MockPreviousModule(endAddress=0x0)
        self.set_collection = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = Module(self.test_space, self.set_collection)

        self.under_test.previousElement = self.previous_module

        self.observer = MockObserver()
        self.under_test.sizeChangeNotifier.addObserver(self.observer)
        self.under_test.addressChangeNotifier.addObserver(self.observer)

    def test_span_after_previous_address_change(self, this_context):
        r1 = self.under_test.addRegister("r1")
        r1["constraints"]["fixedAddress"] = 0x15

        self.previous_module.endAddress = 0x10
        # Because of the fixedAddress constraint on the register, the module span is implicitly a function of the
        # base address and the fixed address constraint.
        assert 0x11 == self.under_test.baseAddress
        assert 5 == self.under_test.spanMemoryUnits
