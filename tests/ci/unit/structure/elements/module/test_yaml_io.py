#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import logging

import pytest

from registermap.structure.elements.module.module import Module, ParseError
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set import SetCollection

from ..mock_observer import MockObserver
from .mocks import MockPreviousModule

log = logging.getLogger(__name__)


class TestModuleYamlLoadSave:
    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.previous_module = MockPreviousModule(endAddress=0x0)
        self.set_collection = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = Module(self.test_space, self.set_collection)

        self.under_test.previousElement = self.previous_module
        self.under_test["name"] = "module"
        self.under_test.sizeChangeNotifier.addObserver(self.observer)

        self.set_collection.moduleSet.add(self.under_test)

        self.acquired_set_collection = SetCollection()

    def test_encode_decode(self, this_context):
        self.under_test["constraints"]["fixedAddress"] = 0x10
        self.create_register("r1")

        encoded_yaml_data = self.under_test.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decoded_module = Module.from_yamlData(
            encoded_yaml_data, self.test_space, self.acquired_set_collection
        )

        assert (
            decoded_module["constraints"]["fixedAddress"]
            == self.under_test["constraints"]["fixedAddress"]
        )
        assert decoded_module["description"] == self.under_test["description"]
        assert decoded_module["summary"] == self.under_test["summary"]

        assert len(self.acquired_set_collection.moduleSet) == 1

        decoded_module.previousElement = self.previous_module
        # No exceptions should be thrown
        decoded_module.reviewAddressChange()

    def create_register(self, name):
        self.under_test.addRegister(name)

        self.under_test["registers"][name]["constraints"]["fixedAddress"] = 0x10
        self.under_test["registers"][name]["description"] = "some description"
        self.under_test["registers"][name]["mode"] = "ro"
        self.under_test["registers"][name]["public"] = False
        self.under_test["registers"][name]["summary"] = "a summary"

        self.under_test["registers"][name].addField("f1", [3, 5], (3, 5))
        self.under_test["registers"][name].addField("f2", [7, 7], (7, 7))

    def test_from_bad_yaml_data(self, this_context):
        yaml_data = {"mode": "ro"}

        with pytest.raises(
            ParseError, match="^Yaml data does not specify module"
        ):
            Module.from_yamlData(
                yaml_data, self.test_space, self.acquired_set_collection
            )

    def test_from_optional_yaml_data(self, this_context):
        yaml_data = {"mode": "ro"}

        decoded_module = Module.from_yamlData(
            yaml_data,
            self.test_space,
            self.acquired_set_collection,
            optional=True,
        )

        assert len(decoded_module["constraints"]) == 0
        assert decoded_module["description"] == ""
        assert decoded_module["name"] is None
        assert len(decoded_module["registers"]) == 0
        assert decoded_module["summary"] == ""


class TestModuleYamlParameters:
    @pytest.fixture()
    def this_context(self):
        self.previous_module = MockPreviousModule(endAddress=0x215)
        self.set_collection = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = Module(self.test_space, self.set_collection)

        self.under_test.previousElement = self.previous_module

    def test_yaml_data_address(self, this_context):
        # The address data is automatically generated so it is prefixed by '_'.
        assert self.previous_module.endAddress == 0x215

        expected_name = "_address"
        expected_value = 0x216

        assert self.under_test.baseAddress == expected_value

        yaml_data = self.under_test.to_yamlData()
        assert yaml_data["module"][expected_name] == expected_value

    def test_yaml_data_span(self, this_context):
        # The address data is automatically generated so it is prefixed by '_'.
        expected_name = "_spanMemoryUnits"
        expected_value = 0

        assert self.under_test.spanMemoryUnits == expected_value

        yaml_data = self.under_test.to_yamlData()
        assert yaml_data["module"][expected_name] == expected_value

    def test_yaml_data_none_address(self, this_context):
        test_module = Module(self.test_space, self.set_collection)

        yaml_data = test_module.to_yamlData()
        assert yaml_data["module"]["_address"] is None


class TestLoadSaveUserDefinedParameters:
    @pytest.fixture()
    def this_context(self):
        self.previous_module = MockPreviousModule(endAddress=0x0)
        self.set_collection = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = Module(self.test_space, self.set_collection)

        self.under_test.previousElement = self.previous_module
        self.under_test["name"] = "module"

        self.set_collection.moduleSet.add(self.under_test)

        self.acquired_set_collection = SetCollection()

    def test_encode_decode(self, this_context):
        expected_value = "some value"

        self.under_test["my-parameter"] = expected_value

        encoded_yaml_data = self.under_test.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decodeddecoded_moduleodule = Module.from_yamlData(
            encoded_yaml_data, self.test_space, self.acquired_set_collection
        )

        assert expected_value == decodeddecoded_moduleodule["my-parameter"]


class TestModuleYamlAddressRestoration:
    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.previous_module = MockPreviousModule(endAddress=0x0)
        self.set_collection = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = Module(self.test_space, self.set_collection)

        self.under_test.previousElement = self.previous_module
        self.under_test["name"] = "module"
        self.under_test.sizeChangeNotifier.addObserver(self.observer)

        self.set_collection.moduleSet.add(self.under_test)

        self.acquired_set_collection = SetCollection()

    def test_encode_decode_addresses_restored(self, this_context):
        self.under_test["constraints"]["fixedAddress"] = 0x10

        r1 = self.under_test.addRegister("r1")

        r1.addField("f1", (5, 11))

        r2 = self.under_test.addRegister("r2")

        r2.addField("f2", (3, 5))
        r2.addField("f3", (6, 7))

        encoded_yaml_data = self.under_test.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decoded_module = Module.from_yamlData(
            encoded_yaml_data, self.test_space, self.acquired_set_collection
        )

        # This is needed to have the sizes from the export registered correctly.
        decoded_module.reviewAddressChange()
        decoded_module.reviewSizeChange()

        assert (
            self.under_test["constraints"]["fixedAddress"]
            == decoded_module["constraints"]["fixedAddress"]
        )
        assert self.under_test["description"] == decoded_module["description"]
        assert self.under_test["summary"] == decoded_module["summary"]

        assert 1 == len(self.acquired_set_collection.moduleSet)

        decoded_r1 = decoded_module["registers"]["r1"]
        assert r1.startAddress == decoded_r1.startAddress

        decoded_r2 = decoded_module["registers"]["r2"]
        assert r2.startAddress == decoded_r2.startAddress
