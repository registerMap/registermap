#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.elements.module.module import Module
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set import SetCollection

from ..mock_observer import MockObserver
from .mocks import MockPreviousModule


class TestModuleMultipleInstances:
    @pytest.fixture()
    def this_context(self):
        self.previous_module = MockPreviousModule(endAddress=0x10)

        self.sizeObserver = MockObserver()
        self.address_observer = MockObserver()
        self.set_collection = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = Module(self.test_space, self.set_collection)
        self.under_test["name"] = "module"

        self.under_test.sizeChangeNotifier.addObserver(self.sizeObserver)
        self.under_test.addressChangeNotifier.addObserver(self.address_observer)

        self.under_test.previousElement = self.previous_module

        r1 = self.under_test.addRegister("r1")

        r1.addField("r1f1", (2, 4))
        r1.addField("r1f2", (5, 7))

        r2 = self.under_test.addRegister("r2")

        r2.addField("r2f1", (2, 9))
        r2.addField("r2f2", (13, 18))

    def test_single_instance_span(self, this_context):
        assert 1 == self.under_test["instances"]

        expected_size = 4

        assert expected_size == self.under_test.spanMemoryUnits

    def test_multiple_instance_continuous_span(self, this_context):
        # Module in the series are immediately adjacent to each other.
        assert 1 == self.under_test["instances"]

        single_instance_size = self.under_test.spanMemoryUnits

        self.under_test["instances"] = 4
        assert 4 == self.under_test["instances"]

        expected_multiple_instance_size = (
            self.under_test["instances"] * single_instance_size
        )

        assert (
            expected_multiple_instance_size == self.under_test.spanMemoryUnits
        )

    def test_instance_change_notifies_observers(self, this_context):
        assert 4 == self.sizeObserver.update_count
        assert 1 == self.address_observer.update_count

        self.under_test["instances"] = 10

        assert 5, self.sizeObserver.update_count
        assert 2, self.address_observer.update_count


class TestModuleInstanceAddresses:
    @pytest.fixture()
    def this_context(self):
        self.previous_module = MockPreviousModule(endAddress=0x10)

        self.observer = MockObserver()
        self.set_collection = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = Module(self.test_space, self.set_collection)
        self.under_test["name"] = "m1"

        self.under_test.sizeChangeNotifier.addObserver(self.observer)
        self.under_test.addressChangeNotifier.addObserver(self.observer)

        self.under_test.previousElement = self.previous_module

        self.m2 = Module(self.test_space, self.set_collection)
        self.m2.previousElement = self.under_test

    def test_default_multiple_instance_address(self, this_context):
        """
        Since a module has no size until registers are added, changing the number of instances consumes doesn't change
        the addresses.
        """
        assert 0x11 == self.under_test.baseAddress
        assert 0x11 == self.m2.baseAddress

        self.under_test["instances"] = 4

        assert 0x11 == self.under_test.baseAddress
        assert 0x11 == self.m2.baseAddress


class TestModuleInstancesConstraints:
    @pytest.fixture()
    def this_context(self):
        self.mock_previous_module = MockPreviousModule(endAddress=0x10)

        self.observer = MockObserver()
        self.set_collection = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = Module(self.test_space, self.set_collection)
        self.under_test["name"] = "module"

        self.under_test.sizeChangeNotifier.addObserver(self.observer)
        self.under_test.addressChangeNotifier.addObserver(self.observer)

        self.under_test.previousElement = self.mock_previous_module

        r1 = self.under_test.addRegister("r1")

        r1.addField("r1f1", (2, 4))
        r1.addField("r1f2", (5, 7))

        r2 = self.under_test.addRegister("r2")

        r2.addField("r2f1", (2, 10))

        assert 3 == self.under_test.spanMemoryUnits

    def test_alignment_constrained_instances_span(self, this_context):
        assert 0x10 == self.mock_previous_module.endAddress

        # Prior to instances or alignment the span is the size of a single module.
        assert 3 == self.under_test.spanMemoryUnits

        number_instances = 4

        # Applying instances and the size is the instance multiple of the single module size.

        self.under_test["instances"] = number_instances

        assert 12 == self.under_test.spanMemoryUnits

        alignment = 4

        self.under_test["constraints"]["alignmentMemoryUnits"] = alignment

        # 15 = (3 * 4 [span caused by alignment]) + 3 [last instance span]
        expected_instances_span = 15
        assert expected_instances_span == self.under_test.spanMemoryUnits

    def test_fixed_size_constrained_instances_span(self, this_context):
        expected_module_size = 10
        number_instances = 4

        self.under_test["constraints"][
            "fixedSizeMemoryUnits"
        ] = expected_module_size

        assert expected_module_size == self.under_test.spanMemoryUnits

        self.under_test["instances"] = number_instances

        expected_instances_span = expected_module_size * number_instances
        assert expected_instances_span == self.under_test.spanMemoryUnits

    def test_fixed_size_and_alignment_instances_span(self, this_context):
        number_instances = 4
        expected_module_size = 5

        self.under_test["constraints"]["alignmentMemoryUnits"] = 4
        self.under_test["constraints"][
            "fixedSizeMemoryUnits"
        ] = expected_module_size

        # Expect 8 memory units to be consumed by each module in the series, except the last module.
        # The next module could start at the next available memory unit, unless it also has an alignment constraint (which
        # would be sensible, but not necessary).

        assert expected_module_size == self.under_test.spanMemoryUnits

        self.under_test["instances"] = number_instances

        expected_instances_span = (
            8 * (number_instances - 1) + expected_module_size
        )
        assert expected_instances_span == self.under_test.spanMemoryUnits
