#
# Copyright 2017 Russell Smiley
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.elements.base import ElementList
from registermap.structure.elements.module.module import (
    Module,
    ParseError,
    RegisterInstance,
)
from registermap.structure.elements.module.parameters import RegistersParameter
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set import SetCollection


class TestModuleRegistersParameter:
    @pytest.fixture()
    def this_context(self):
        self.source_collection = SetCollection()
        self.memory_space = MemoryConfiguration()
        self.module = Module(self.memory_space, self.source_collection)

        self.acquired_collection = SetCollection()

    def test_initialization(self, this_context):
        p = RegistersParameter(self.module)

        assert p.name == "registers"
        assert isinstance(p.value, ElementList)

    def test_empty_bit_fields_to_yaml_data(self, this_context):
        p = RegistersParameter(self.module)

        expected_yaml_data = {"registers": list()}
        actual_yaml_data = p.to_yamlData()

        assert actual_yaml_data == expected_yaml_data

    def test_single_register_to_yaml_data(self, this_context):
        p = RegistersParameter(self.module)
        p.value["r1"] = self.create_register("r1")

        expected_yaml_data = {"registers": [p.value["r1"].to_yamlData()]}
        actual_yaml_data = p.to_yamlData()

        assert actual_yaml_data == expected_yaml_data

    def test_multiple_registers_to_yaml_data(self, this_context):
        p = RegistersParameter(self.module)
        p.value["r1"] = self.create_register("r1")
        p.value["r2"] = self.create_register("r2", 0x12)

        expected_yaml_data = {
            "registers": [
                p.value["r1"].to_yamlData(),
                p.value["r2"].to_yamlData(),
            ]
        }
        actual_yaml_data = p.to_yamlData()

        assert actual_yaml_data == expected_yaml_data

    def create_register(self, name, fixedAddress=0x10) -> RegisterInstance:
        register = RegisterInstance(
            self.memory_space, setCollection=self.source_collection
        )
        register["constraints"]["fixedAddress"] = fixedAddress
        register["description"] = "some description"
        register["mode"] = "ro"
        register["name"] = name
        register["public"] = False
        register["summary"] = "a summary"

        register.addField("f1", [3, 5], (0, 2))
        register.addField("f2", [7, 7], (0, 0))

        return register

    def test_from_good_yaml_data(self, this_context):
        p = RegistersParameter(self.module)
        p.value["r1"] = self.create_register("r1")
        p.value["r2"] = self.create_register("r2", 0x12)

        yaml_data = p.to_yamlData()
        gp = RegistersParameter.from_yamlData(
            yaml_data, self.module, self.memory_space, self.acquired_collection
        )

        assert gp.value["r1"]["fields"]["f1"]["name"] == "f1"
        assert gp.value["r1"]["fields"]["f1"]["size"] == 3
        assert gp.value["r1"]["fields"]["f2"]["name"] == "f2"
        assert gp.value["r1"]["fields"]["f2"]["size"] == 1

        assert gp.value["r2"]["fields"]["f1"]["name"] == "f1"
        assert gp.value["r2"]["fields"]["f1"]["size"] == 3
        assert gp.value["r2"]["fields"]["f2"]["name"] == "f2"
        assert gp.value["r2"]["fields"]["f2"]["size"] == 1

    def test_from_bad_yaml_data(self, this_context):
        yaml_data = {"mode": "ro"}
        with pytest.raises(
            ParseError, match="^Registers not defined in yaml data"
        ):
            RegistersParameter.from_yamlData(
                yaml_data,
                self.module,
                self.memory_space,
                self.acquired_collection,
            )

    def test_optional_yaml_data(self, this_context):
        yaml_data = {"mode": "ro"}
        gp = RegistersParameter.from_yamlData(
            yaml_data,
            self.module,
            self.memory_space,
            self.source_collection,
            optional=True,
        )
