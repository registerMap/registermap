#
# Copyright 2017 Russell Smiley
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.constraints.constraintTable import ConstraintTable
from registermap.structure.elements.base.parameter import (
    ConstraintsParameter,
    Parameter,
    ParseError,
)
from registermap.structure.memory.configuration import MemoryConfiguration


class TestParameter:
    def test_initialization(self):
        expected_name = "someName"
        expected_value = 31415
        p = Parameter(expected_name, expected_value)

        assert p.name == expected_name
        assert p.value == expected_value

    def test_to_yaml_data(self):
        expected_name = "someName"
        expected_value = 31415
        p = Parameter(expected_name, expected_value)

        expected_yaml_data = {expected_name: expected_value}
        actual_yaml_data = p.to_yamlData()

        assert actual_yaml_data == expected_yaml_data

    def test_from_good_yaml_data(self):
        expected_name = "someName"
        expected_value = 31415
        yaml_data = {expected_name: expected_value}

        actual_parameter = Parameter.from_yamlData(yaml_data, expected_name)

        assert actual_parameter.name == expected_name
        assert actual_parameter.value == expected_value

    def test_from_bad_yaml_data_raises(self):
        expected_name = "someName"
        expected_value = 31415
        yaml_data = {"badname": expected_value}

        with pytest.raises(ParseError, match="^Parameter is not in yaml data"):
            Parameter.from_yamlData(yaml_data, expected_name)

    def test_optional_from_yaml_data_no_raise(self):
        expected_name = "someName"
        expected_value = 31415
        yaml_data = {"othername": expected_value}

        actual_parameter = Parameter.from_yamlData(
            yaml_data, expected_name, optional=True
        )

        assert actual_parameter.name == expected_name
        assert actual_parameter.value is None


class TestConstraintsParameter:
    @pytest.fixture()
    def this_context(self):
        self.memory = MemoryConfiguration()

    def test_initialization(self, this_context):
        p = ConstraintsParameter(self.memory)

        assert p.name == "constraints"
        assert isinstance(p.value, ConstraintTable)

    def test_to_yaml_data(self, this_context):
        p = ConstraintsParameter(self.memory)

        p.value["fixedAddress"] = 0x10
        p.value["fixedSizeMemoryUnits"] = 5
        p.value["alignmentMemoryUnits"] = 2
        expected_yaml_data = {
            "constraints": {
                "fixedAddress": 0x10,
                "fixedSize": 5,
                "alignment": 2,
            }
        }
        actual_yaml_data = p.to_yamlData()

        assert actual_yaml_data == expected_yaml_data

    def test_from_good_yaml_data(self, this_context):
        p = ConstraintsParameter(self.memory)

        p.value["fixedAddress"] = 0x10
        p.value["fixedSizeMemoryUnits"] = 5
        p.value["alignmentMemoryUnits"] = 2

        yaml_data = p.to_yamlData()
        generated_parameter = ConstraintsParameter.from_yamlData(
            yaml_data, self.memory
        )

        assert (
            p.value["fixedAddress"] == generated_parameter.value["fixedAddress"]
        )
        assert (
            p.value["fixedSizeMemoryUnits"]
            == generated_parameter.value["fixedSizeMemoryUnits"]
        )
        assert (
            p.value["alignmentMemoryUnits"]
            == generated_parameter.value["alignmentMemoryUnits"]
        )

    def test_good_data_with_other_parameter(self, this_context):
        p = ConstraintsParameter(self.memory)

        p.value["fixedAddress"] = 0x10
        p.value["fixedSizeMemoryUnits"] = 5
        p.value["alignmentMemoryUnits"] = 2

        yaml_data = p.to_yamlData()
        yaml_data["mode"] = "ro"
        generated_parameter = ConstraintsParameter.from_yamlData(
            yaml_data, self.memory
        )

        assert (
            p.value["fixedAddress"] == generated_parameter.value["fixedAddress"]
        )
        assert (
            p.value["fixedSizeMemoryUnits"]
            == generated_parameter.value["fixedSizeMemoryUnits"]
        )
        assert (
            p.value["alignmentMemoryUnits"]
            == generated_parameter.value["alignmentMemoryUnits"]
        )

    def test_bad_yaml_data_raises(self, this_context):
        yaml_data = {"mode": "ro"}

        with pytest.raises(
            ParseError, match="^Yaml data does not specify constraints"
        ):
            ConstraintsParameter.from_yamlData(yaml_data, self.memory)

    def test_optional_yaml_data(self, this_context):
        yaml_data = {"mode": "ro"}
        generated_parameter = ConstraintsParameter.from_yamlData(
            yaml_data, self.memory, optional=True
        )
        assert len(generated_parameter.value) == 0
