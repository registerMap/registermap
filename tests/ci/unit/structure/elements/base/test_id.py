#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.elements.base.identity import IdentityElement


class MockIdentityElement(IdentityElement):
    """
    Implement abstract methods to enable testing.
    """

    @property
    def canonicalId(self):
        return None


class TestIdentityElement:
    @pytest.fixture()
    def this_context(self):
        self.first_id = MockIdentityElement()
        self.second_id = MockIdentityElement()

    def test_initial_id(self, this_context):
        first_id = self.first_id.id
        second_id = self.second_id.id

        assert first_id != second_id

    def test_id_consistency(self, this_context):
        first_id_read = self.first_id.id

        new_id = MockIdentityElement()

        second_id_read = self.first_id.id

        assert first_id_read == second_id_read
