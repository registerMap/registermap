#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.elements.field.field import Field


class TestFieldUserDefinedParameter:
    @pytest.fixture()
    def this_context(self):
        self.under_test = Field()

    def test_assign_parameter_ok(self, this_context):
        expected_value = "some value"
        self.under_test["my-parameter"] = expected_value

        assert expected_value == self.under_test["my-parameter"]

    def test_bad_parameter_raises(self, this_context):
        with pytest.raises(
            KeyError, match="Field parameter not in core or user data"
        ):
            self.under_test["bad-parameter"]

    def test_underscore_prefix_asserts(self, this_context):
        with pytest.raises(AssertionError):
            self.under_test["_my-parameter"] = 2
