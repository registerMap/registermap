#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.elements.field.parameters import (
    BooleanParameter,
    ConfigurationError,
)


class TestBooleanParameter:
    @pytest.fixture()
    def this_context(self):
        self.p = BooleanParameter("booleanParameter")
        assert self.p.value

    def test_data_assignment(self, this_context):
        self.p.value = False

        assert not self.p.value

    def test_non_bool_raises(self, this_context):
        with pytest.raises(
            ConfigurationError, match="^Must be specified as boolean"
        ):
            self.p.value = "true"
