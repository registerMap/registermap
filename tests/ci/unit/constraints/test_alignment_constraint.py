#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import logging

import pytest

from registermap.constraints.constraints import (
    AbstractConstraint,
    AlignmentMemoryUnits,
    ConstraintError,
    ParseError,
)
from registermap.structure.memory.configuration import MemoryConfiguration

log = logging.getLogger(__name__)


@pytest.fixture()
def this_memory() -> MemoryConfiguration:
    return MemoryConfiguration()


@pytest.fixture()
def constraint_under_test(
    this_memory: MemoryConfiguration,
) -> AlignmentMemoryUnits:
    constraint = AlignmentMemoryUnits(this_memory)

    return constraint


class TestAlignmentMemoryUnits:
    def test_name(self, constraint_under_test: AlignmentMemoryUnits):
        assert constraint_under_test.name == AlignmentMemoryUnits.name

    def test_type(self, constraint_under_test: AlignmentMemoryUnits):
        assert (
            constraint_under_test.type
            == AbstractConstraint.constraintTypes["address"]
        )


def do_none_constraint_value_asserts(method_under_test):
    with pytest.raises(AssertionError):
        method_under_test(0xA)


def do_non_int_address_raises(method_under_test):
    with pytest.raises(AssertionError):
        method_under_test("0xa")


def do_negative_address_raises(method_under_test):
    with pytest.raises(AssertionError):
        method_under_test("-12")


def do_zero_address_no_raise(method_under_test):
    method_under_test(0)


class TestAlignmentMemoryUnitsConstructor:
    def test_default_constructor_no_raise(self, this_memory):
        c = AlignmentMemoryUnits(this_memory)

    def test_zero_constraint_value_raises(self, this_memory):
        with pytest.raises(
            ConstraintError,
            match="^Alignment must be a positive non-zero integer",
        ):
            AlignmentMemoryUnits(this_memory, constraintValue=0)

    def test_non_int_constraint_value_asserts(self, this_memory):
        with pytest.raises(AssertionError):
            AlignmentMemoryUnits(this_memory, constraintValue="1")

    def test_none_constraint_value_no_raise(self, this_memory):
        c = AlignmentMemoryUnits(this_memory, constraintValue=None)


class TestAlignmentMemoryUnitsCalculation:
    def test_none_constraint_asserts(
        self, constraint_under_test: AlignmentMemoryUnits
    ):
        do_none_constraint_value_asserts(constraint_under_test.calculate)

    def test_non_int_address_raises(
        self, constraint_under_test: AlignmentMemoryUnits
    ):
        do_non_int_address_raises(constraint_under_test.calculate)

    def test_negative_address_raises(
        self, constraint_under_test: AlignmentMemoryUnits
    ):
        do_negative_address_raises(constraint_under_test.calculate)

    def test_zero_address_no_raise(
        self, constraint_under_test: AlignmentMemoryUnits
    ):
        constraint_under_test.value = 4
        do_zero_address_no_raise(constraint_under_test.calculate)

    def test_align_address(self, constraint_under_test: AlignmentMemoryUnits):
        expected_address = 0xC
        constraint_under_test.value = 4

        actual_address = constraint_under_test.calculate(0xA)

        assert actual_address == expected_address

    def test_none_address(self, constraint_under_test: AlignmentMemoryUnits):
        constraint_under_test.value = 4

        assert constraint_under_test.calculate(None) is None

    def test_zero_address(self, constraint_under_test: AlignmentMemoryUnits):
        expected_address = 0
        constraint_under_test.value = 4

        actualAddress = constraint_under_test.calculate(expected_address)

        assert actualAddress == expected_address


class TestAlignmentMemoryUnitsValidation:
    def test_non_int_address_raises(
        self, constraint_under_test: AlignmentMemoryUnits
    ):
        do_non_int_address_raises(constraint_under_test.validate)

    def test_negative_address_raises(
        self, constraint_under_test: AlignmentMemoryUnits
    ):
        do_negative_address_raises(constraint_under_test.validate)

    def test_zero_address_no_raise(
        self, constraint_under_test: AlignmentMemoryUnits
    ):
        do_zero_address_no_raise(constraint_under_test.validate)


class TestAlignmentMemoryUnitsValueProperty:
    def testGetProperty(self, constraint_under_test: AlignmentMemoryUnits):
        expected_value = 14
        constraint_under_test.value = expected_value

        actual_value = constraint_under_test.value

        assert actual_value == expected_value

    def testZeroValueRaises(self, constraint_under_test: AlignmentMemoryUnits):
        with pytest.raises(
            ConstraintError,
            match="^Alignment must be a positive non-zero integer",
        ):
            constraint_under_test.value = 0

    def testNonIntValueAsserts(
        self, constraint_under_test: AlignmentMemoryUnits
    ):
        with pytest.raises(AssertionError):
            constraint_under_test.value = "1"


class TestLoadSave:
    def test_encode_decode(
        self,
        constraint_under_test: AlignmentMemoryUnits,
        this_memory: MemoryConfiguration,
    ):
        constraint_under_test.value = 4

        encoded_yaml_data = constraint_under_test.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decoded_data = AlignmentMemoryUnits.from_yamlData(
            encoded_yaml_data, this_memory
        )

        assert decoded_data.value == constraint_under_test.value

    def test_decode_non_alignment_constraint_raises(
        self, this_memory: MemoryConfiguration
    ):
        with pytest.raises(
            ParseError, match="^Yaml data does not contain alignment constraint"
        ):
            AlignmentMemoryUnits.from_yamlData({}, this_memory)

    def test_decode_optional_alignment_constraint_raises(
        self, this_memory: MemoryConfiguration
    ):
        constraint = AlignmentMemoryUnits.from_yamlData(
            {}, this_memory, optional=True
        )

        assert constraint is None
