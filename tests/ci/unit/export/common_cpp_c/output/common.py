#
# Copyright 2018 Russell Smiley
#
# This file is part of registermap.
#
# registermap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registermap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

import io
import unittest.mock


class KeepString(io.StringIO):
    """
    Retain the lines written to io.StringIO before it is closed (at which point they are discarded).
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.output_lines = None

    def close(self):
        self.seek(0)
        self.output_lines = self.readlines()

        super().close()


def do_template_test(
    output_under_test,
    open_module_path,
    create_directory_module_path,
    suppress_module_path=None,
):
    """
    Acquire the output
    :param output_under_test:
    :param open_module_path:
    :param create_directory_module_path:
    :param suppress_module_path:
    """
    mock_file_object = KeepString()
    with unittest.mock.patch(
        create_directory_module_path, return_value=True
    ), unittest.mock.patch(
        open_module_path, unittest.mock.mock_open()
    ) as mock_memory_open:
        mock_memory_open.return_value = mock_file_object

        if suppress_module_path is None:
            # apply the output for testing
            output_under_test.apply()
        else:
            # mock the module path to be suppresed.
            with unittest.mock.patch(suppress_module_path):
                output_under_test.apply()

    return mock_file_object.output_lines
