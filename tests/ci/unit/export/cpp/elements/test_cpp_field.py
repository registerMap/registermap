#
# Copyright 2018 Russell Smiley
#
# This file is part of registermap.
#
# registermap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registermap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.export.cpp.elements.field import Field


class TestCppExportField:
    @pytest.fixture()
    def elements_context(self):
        self.mock_field_element = {
            "size": 0,
        }

        self.field_under_test = Field(self.mock_field_element)

    def test_valid_types(self, elements_context):
        expected_values = [
            {
                "size": 8,
                "type": "std::uint8_t",
            },
            {
                "size": 9,
                "type": "std::uint16_t",
            },
            {
                "size": 16,
                "type": "std::uint16_t",
            },
            {
                "size": 17,
                "type": "std::uint32_t",
            },
            {
                "size": 32,
                "type": "std::uint32_t",
            },
            {
                "size": 33,
                "type": "std::uint64_t",
            },
            {
                "size": 64,
                "type": "std::uint64_t",
            },
        ]
        for this_value in expected_values:
            self.mock_field_element["size"] = this_value["size"]

            assert this_value["type"] == self.field_under_test.type

    def test_type_greater_than64_asserts(self, elements_context):
        # Assume that 64 bit is the maximum field size type allowed.
        self.mock_field_element["size"] = 65

        with pytest.raises(AssertionError):
            self.field_under_test.type
