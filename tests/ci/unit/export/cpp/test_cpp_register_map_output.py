#
# Copyright 2018 Russell Smiley
#
# This file is part of registermap.
#
# registermap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registermap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap import RegisterMap
from registermap.export.cpp.options import CppOptions
from registermap.export.cpp.registerMap import Output as CppOutput

BASE_MODULE_PATH = "registermap.export.cpp.registerMap"


class TestCRegisterMapHeaderSource:
    @pytest.fixture()
    def elements_context(self):
        self.outputDir = "some/path"

        self.__setup_register_map()

        self.mock_options = CppOptions()
        self.mock_options.includePrefix = "this/prefix"
        self.mock_options.output = self.outputDir
        self.mock_options.packAlignment = 10

    def __setup_register_map(self):
        self.registerMap = RegisterMap()

        self.registerMap.memory.baseAddress = 0x20F0

        m1 = self.registerMap.addModule("m1")
        r1 = m1.addRegister("r1")
        r1.addField("f1", (0, 11))

        m2 = self.registerMap.addModule("m2")
        r2 = m2.addRegister("r2")
        r2.addField("f2", (0, 4))

        m3 = self.registerMap.addModule("m3")
        r3 = m3.addRegister("r3")
        r3.addField("f3", (4, 6))

    def test_execution(self, elements_context, mocker):
        mocker.patch(
            "{0}.OutputBase._OutputBase__validateOutputDirectory".format(
                BASE_MODULE_PATH
            )
        )
        mock_macro_template = mocker.patch(
            "registermap.export.cpp.registerMap.MacroTemplates".format(
                BASE_MODULE_PATH
            )
        )
        mock_memory_template = mocker.patch(
            "{0}.MemoryTemplates".format(BASE_MODULE_PATH)
        )
        mock_module_template = mocker.patch(
            "{0}.ModuleTemplates".format(BASE_MODULE_PATH)
        )
        mock_register_map_template = mocker.patch(
            "{0}.RegisterMapTemplates".format(BASE_MODULE_PATH)
        )
        mocker.patch(
            "{0}.Output._Output__createDirectories".format(BASE_MODULE_PATH)
        )
        output_under_test = CppOutput(self.mock_options)

        # Mock created directory paths
        output_under_test.includePath = "some/include"
        output_under_test.sourceDirectory = "some/source"

        output_under_test.generate(self.registerMap, "myRegisterMap")

        mock_macro_template_instance = mock_macro_template.return_value
        mock_macro_template_instance.apply.assert_called_once()

        mock_memory_template_instance = mock_memory_template.return_value
        mock_memory_template_instance.apply.assert_called_once()

        mock_module_template_instance = mock_module_template.return_value
        mock_module_template_instance.apply.assert_called_once()

        mock_registermap_template_instance = (
            mock_register_map_template.return_value
        )
        mock_registermap_template_instance.apply.assert_called_once()
