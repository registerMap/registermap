#
# Copyright 2018 Russell Smiley
#
# This file is part of registermap.
#
# registermap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registermap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

import copy
import os

import jinja2
import pytest

import registermap.export.base.template
from registermap.export.cpp.elements import Memory
from registermap.export.cpp.output.memory import (
    MEMORY_TEMPLATE_CONFIGURATION,
    MemoryTemplates,
)
from tests.ci.unit.export.common_cpp_c.output import do_template_test

CREATE_DIRECTORY_MODULE_PATH = (
    "registermap.export.cpp.output.memory.MemoryTemplates.createDirectory"
)
OPEN_MODULE_PATH = "registermap.export.commonCppC.output.memory.open"


class TestMemoryTemplates:
    @pytest.fixture()
    def elements_context(self, mocker):
        self.expected_name = "someName"
        self.include_directory = "include/path"
        self.source_directory = "source/path"

        self.mock_memory = mocker.create_autospec(Memory)
        self.mock_memory.baseAddress = 1234
        self.mock_memory.size = 345
        self.mock_memory.sizeType = "uint_least8_t"

        self.template_config = copy.deepcopy(MEMORY_TEMPLATE_CONFIGURATION)

    def test_defaults(self, elements_context):
        expected_memory_directory = os.path.join(
            self.include_directory, "memory"
        )
        expected_template_directory = os.path.join("templates", "idiomatic")

        paths = MemoryTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.sourceDirectory = self.source_directory

        output_under_test = MemoryTemplates(
            paths, self.expected_name, self.mock_memory
        )

        assert self.expected_name == output_under_test.registerMapName
        assert (
            expected_template_directory == output_under_test.templateDirectory
        )
        assert expected_memory_directory == output_under_test.memoryDirectory
        assert self.mock_memory == output_under_test.encapsulatedMemory

    def test_initial_created_files(self, elements_context, mocker):
        paths = MemoryTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.sourceDirectory = self.source_directory

        output_under_test = MemoryTemplates(
            paths, self.expected_name, self.mock_memory
        )
        mock_template = mocker.create_autospec(jinja2.Template)
        mock_create_directory = mocker.patch(
            CREATE_DIRECTORY_MODULE_PATH, return_value=True
        )
        mocker.patch(OPEN_MODULE_PATH)
        mocker.patch.object(
            registermap.export.base.template.jinja2.Environment,
            "get_template",
            return_value=mock_template,
        )
        output_under_test.apply()

        expected_files = [
            "include/path/memory/memory.hpp",
            "source/path/memory.cpp",
        ]

        assert expected_files == output_under_test.createdFiles

        assert 2 == len(mock_template.render.mock_calls)

        mock_create_directory.assert_has_calls(
            [
                mocker.call(output_under_test.memoryDirectory),
            ]
        )

    def test_memory_header(self, elements_context):
        paths = MemoryTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.sourceDirectory = self.source_directory

        output_under_test = MemoryTemplates(
            paths, self.expected_name, self.mock_memory
        )
        # Select the header to test
        self.template_config["header"] = [self.template_config["header"][0]]
        self.template_config["source"] = list()
        output_under_test.configuration = self.template_config

        actual_lines = do_template_test(
            output_under_test, OPEN_MODULE_PATH, CREATE_DIRECTORY_MODULE_PATH
        )
        expected_lines = [
            "/*\n",
            " *\n",
            " * someName\n",
            " *\n",
            " *\n",
            " */\n",
            "\n",
            "#ifndef SOMENAME_MEMORY_HPP\n",
            "#define SOMENAME_MEMORY_HPP\n",
            "\n",
            "#include <cstdint>\n",
            "\n",
            "\n",
            "namespace someName\n",
            "{\n",
            "\n",
            "class MemorySpace\n",
            "  {\n",
            "  public:\n",
            "\n",
            "    static uint_least8_t volatile* const\n",
            "      base;\n",
            "\n",
            "#ifdef OFF_TARGET_MEMORY\n",
            "\n",
            "    static constexpr std::uint_least32_t\n",
            "      allocated_memory_span = 345;\n",
            "\n",
            "    static uint_least8_t volatile\n",
            "      off_target_memory[ allocated_memory_span ];\n",
            "\n",
            "#endif\n",
            "\n",
            "  };\n",
            "\n",
            "}\n",
            "\n",
            "#endif\n",
        ]

        assert expected_lines == actual_lines

    def test_memory_source(self, elements_context):
        paths = MemoryTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.includePrefix = "this/prefix"
        paths.sourceDirectory = self.source_directory

        output_under_test = MemoryTemplates(
            paths, self.expected_name, self.mock_memory
        )
        # Select the source to test
        self.template_config["header"] = list()
        self.template_config["source"] = [self.template_config["source"][0]]
        output_under_test.configuration = self.template_config

        actual_lines = do_template_test(
            output_under_test, OPEN_MODULE_PATH, CREATE_DIRECTORY_MODULE_PATH
        )
        expected_lines = [
            "/*\n",
            " *\n",
            " * someName\n",
            " *\n",
            " *\n",
            " */\n",
            "\n",
            "#include <cstdint>\n",
            "\n",
            '#include "this/prefix/memory/memory.hpp"\n',
            "\n",
            "\n",
            "namespace someName\n",
            "{\n",
            "\n",
            "#ifndef OFF_TARGET_MEMORY\n",
            "\n",
            "  uint_least8_t volatile* const\n",
            "    MemorySpace::base = reinterpret_cast<uint_least8_t volatile* const>( 1234 );\n",
            "\n",
            "#else\n",
            "\n",
            "  constexpr std::uint_least32_t\n",
            "    MemorySpace::allocated_memory_span;\n",
            "\n",
            "  uint_least8_t volatile\n",
            "    MemorySpace::off_target_memory[];\n",
            "\n",
            "  uint_least8_t volatile* const\n",
            "    MemorySpace::base = MemorySpace::off_target_memory;\n",
            "\n",
            "#endif\n",
            "\n",
            "}\n",
        ]

        expected_text = "".join(expected_lines)
        actual_text = "".join(actual_lines)

        assert expected_text == actual_text
