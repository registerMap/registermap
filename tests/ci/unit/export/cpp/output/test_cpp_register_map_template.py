#
# Copyright 2018 Russell Smiley
#
# This file is part of registermap.
#
# registermap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registermap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

import jinja2
import pytest

import registermap.export.base.template
from registermap import RegisterMap as ImportedRegisterMap
from registermap.export.c.elements.registerMap import RegisterMap
from registermap.export.cpp.output.registerMap import RegisterMapTemplates
from tests.ci.unit.export.common_cpp_c.output import do_template_test

CREATE_DIRECTORY_MODULE_PATH = "registermap.export.cpp.output.registerMap.RegisterMapTemplates.createDirectory"
OPEN_MODULE_PATH = "registermap.export.commonCppC.output.registerMap.open"

HEADER_MODULE_PATH = (
    "registermap.export.cpp.output.registerMap.RegisterMapTemplatesBase"
    "._RegisterMapTemplatesBase__createRegisterMapHeader"
)
SOURCE_MODULE_PATH = (
    "registermap.export.cpp.output.registerMap.RegisterMapTemplatesBase"
    "._RegisterMapTemplatesBase__createRegisterMapSource"
)


class TestRegisterMapTemplates:
    @pytest.fixture()
    def elements_context(self):
        self.expected_name = "someName"
        self.include_directory = "include/path"
        self.source_directory = "source/path"

        self.__setup_register_map()

    def __setup_register_map(self):
        registermap = ImportedRegisterMap()

        registermap.memory.baseAddress = 0x20F0

        self.m1 = registermap.addModule("m1")

        r1 = self.m1.addRegister("r1")

        r1.addField("f1", (0, 2))
        r1.addField("f2", (3, 4))
        r1.addField("f3", (5, 15))

        self.register_map = RegisterMap(self.expected_name, registermap)

    def test_initial_created_files(self, elements_context, mocker):
        paths = RegisterMapTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.sourceDirectory = self.source_directory

        output_under_test = RegisterMapTemplates(paths, self.register_map)
        mock_template = mocker.create_autospec(jinja2.Template)
        mocker.patch(CREATE_DIRECTORY_MODULE_PATH, return_value=True)
        mocker.patch(OPEN_MODULE_PATH)
        mocker.patch.object(
            registermap.export.base.template.jinja2.Environment,
            "get_template",
            return_value=mock_template,
        )
        output_under_test.apply()

        expected_files = [
            "include/path/registermap.hpp",
            "source/path/someName.cpp",
        ]

        assert expected_files == output_under_test.createdFiles

    def test_header(self, elements_context):
        expected_lines = [
            "/*\n",
            " *\n",
            " * someName\n",
            " *\n",
            " *\n",
            " */\n",
            "\n",
            "#ifndef SOMENAME_HPP\n",
            "#define SOMENAME_HPP\n",
            "\n",
            '#include "this/prefix/modules/m1.hpp"\n',
            "\n",
            "\n",
            "namespace someName\n",
            "{\n",
            "\n",
            "#pragma pack( 10 )\n",
            "\n",
            "  class someName_t\n",
            "  {\n",
            "    public:\n",
            "      MemorySpace memory;\n",
            "\n",
            "      someName_t() :\n",
            "          m1( reinterpret_cast<m1_t volatile* const>( memory.base + 0x0 ) )\n",
            "        {};\n",
            "\n",
            "      m1_t volatile* const m1;\n",
            "  };\n",
            "\n",
            "#pragma pack()\n",
            "\n",
            "\n",
            "  // Declare the register map instance for users.\n",
            "  extern someName_t someName;\n",
            "\n",
            "}\n",
            "\n",
            "#endif" "\n",
        ]

        paths = RegisterMapTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.includePrefix = "this/prefix"
        paths.sourceDirectory = self.source_directory

        self.register_map.memory.alignment = 10

        output_under_test = RegisterMapTemplates(paths, self.register_map)
        actual_lines = do_template_test(
            output_under_test,
            OPEN_MODULE_PATH,
            CREATE_DIRECTORY_MODULE_PATH,
            suppress_module_path=SOURCE_MODULE_PATH,
        )
        expected_text = "".join(expected_lines)
        actual_text = "".join(actual_lines)
        assert expected_text == actual_text

    def test_source(self, elements_context):
        expected_text = """/*
 *
 * someName
 *
 *
 */

#include "this/prefix/registermap.hpp"


namespace someName
{

  someName_t someName;

}
"""
        paths = RegisterMapTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.includePrefix = "this/prefix"
        paths.sourceDirectory = self.source_directory

        output_under_test = RegisterMapTemplates(paths, self.register_map)
        actual_lines = do_template_test(
            output_under_test,
            OPEN_MODULE_PATH,
            CREATE_DIRECTORY_MODULE_PATH,
            suppress_module_path=HEADER_MODULE_PATH,
        )

        actual_text = "".join(actual_lines)
        assert expected_text == actual_text
