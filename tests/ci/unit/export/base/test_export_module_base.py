#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import unittest

from registermap import RegisterMap

from .mocks import MockField, MockModule, MockRegister


class TestExportModuleBase(unittest.TestCase):
    def setUp(self):
        self.registerMap = RegisterMap()
        self.module1 = self.registerMap.addModule("m1")

        self.export_under_test = MockModule(
            self.module1, MockRegister, MockField
        )

    def test_name(self):
        assert self.module1["name"] == self.export_under_test.name

    def test_empty_module(self):
        assert 0 == len(self.export_under_test.registers)

    def test_hex_address(self):
        assert (
            self.export_under_test.expectedAddress
            == self.export_under_test.address
        )

    def test_offset(self):
        assert (
            self.export_under_test.expectedOffset
            == self.export_under_test.offset
        )

    def test_registers(self):
        self.register1 = self.module1.addRegister("r1")
        self.register1.addField("f1", (0, 2))
        self.register1.addField("f2", (3, 4))
        self.register1.addField("f3", (5, 7))

        self.register2 = self.module1.addRegister("r2")
        self.register2.addField("f1", (0, 2))
        self.register2.addField("f2", (3, 4))
        self.register2.addField("f3", (5, 7))

        for expected_register_name, this_register in zip(
            ["r1", "r2"], self.export_under_test.registers
        ):
            assert expected_register_name == this_register.name

            for expected_field_name, this_field in zip(
                ["f1", "f2", "f3"], this_register.fields
            ):
                assert expected_field_name == this_field.name
