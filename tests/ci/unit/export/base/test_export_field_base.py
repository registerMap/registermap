#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.elements.field import Field as FieldElement

from .mocks import MockField


@pytest.fixture()
def under_test() -> FieldElement:
    u = FieldElement()
    u["name"] = "f1"
    u["size"] = 4

    return u


@pytest.fixture()
def mock_field(under_test) -> MockField:
    m = MockField(under_test)
    return m


class TestExportFieldBase:
    def test_name(self, mock_field, under_test):
        assert under_test["name"] == mock_field.name

    def test_size(self, mock_field, under_test):
        assert under_test["size"] == mock_field.size

    def test_type(self, mock_field, under_test):
        assert mock_field.expectedType == mock_field.type
