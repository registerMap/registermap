#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap import RegisterMap
from registermap.core.interval import sort_intervals
from registermap.export.base.field import FieldBase
from registermap.export.base.register import (
    RegisterContiguousFieldIntervals as Register,
)


class MockField(FieldBase):
    def __init__(self, element):
        super().__init__(element)

    @property
    def type(self):
        return "unsigned"


class TestCppRegisterFieldOrdering:
    @pytest.fixture()
    def elements_context(self):
        self.register_map = RegisterMap()

        m1 = self.register_map.addModule("m1")

        self.r1 = m1.addRegister("r1")

        self.cpp_register_under_test = Register(self.r1, MockField)

    def test_contiguous_local_fields(self, elements_context):
        expected_fields = [
            self.r1.addField("f1", (0, 3)),
            self.r1.addField("f2", (4, 6)),
            self.r1.addField("f3", (7, 7)),
        ]
        assert 8 == self.r1.sizeBits

        register_intervals = sort_intervals(self.r1.bitMap.sourceIntervals)
        fields_under_test = self.cpp_register_under_test.fields

        assert len(register_intervals) == len(fields_under_test)

        assert len(expected_fields) == len(register_intervals)

        for index in range(0, len(register_intervals)):
            expected_interval = register_intervals[index].value
            field_intervals = sort_intervals(
                fields_under_test[
                    index
                ]._element.bitMap.destinationIntervals.values()
            )

            assert 1 == len(field_intervals)
            assert expected_interval == field_intervals[0].value

    def test_noncontiguous_local_fields8_bit_register(self, elements_context):
        self.r1.addField("f1", (0, 1))
        # Unassigned interval (2, 3)
        self.r1.addField("f2", (4, 5))
        # Unassigned interval (6, 6)
        self.r1.addField("f3", (7, 7))

        assert 8 == self.r1.sizeBits

        expected_number_fields = 5

        fields_under_test = self.cpp_register_under_test.fields

        assert expected_number_fields == len(fields_under_test)

        # Can only check names and sizes for inserted gaps
        expected_sizes = [
            2,
            2,
            2,
            1,
            1,
        ]
        actual_sizes = [x._element["size"] for x in fields_under_test]
        assert expected_sizes == actual_sizes

        expected_names = [
            "f1",
            "",
            "f2",
            "",
            "f3",
        ]
        actual_names = [x._element["name"] for x in fields_under_test]
        assert expected_names == actual_names

    def test_noncontiguous_local_fields8_bit_register_starts_with_unassignedf(
        self, elements_context
    ):
        # Unassigned interval (0, 0)
        self.r1.addField("f1", (1, 1))
        # Unassigned interval (2, 3)
        self.r1.addField("f2", (4, 5))
        # Unassigned interval (6, 6)
        self.r1.addField("f3", (7, 7))

        assert 8 == self.r1.sizeBits

        expected_number_fields = 6

        fields_under_test = self.cpp_register_under_test.fields

        assert expected_number_fields == len(fields_under_test)

        # Can only check names and sizes for inserted gaps
        expected_sizes = [
            1,
            1,
            2,
            2,
            1,
            1,
        ]
        actual_sizes = [x._element["size"] for x in fields_under_test]
        assert expected_sizes == actual_sizes

        expected_names = [
            "",
            "f1",
            "",
            "f2",
            "",
            "f3",
        ]
        actual_names = [x._element["name"] for x in fields_under_test]
        assert expected_names == actual_names

    def test_noncontiguous_local_fields16_bit_register(self, elements_context):
        self.r1.addField("f1", (0, 1))
        # Unassigned interval (2, 3)
        self.r1.addField("f2", (4, 5))
        # Unassigned interval (6, 6)
        self.r1.addField("f3", (7, 9))
        # Unassigned interval (10, 15)

        expected_number_fields = 6

        fields_under_test = self.cpp_register_under_test.fields

        assert expected_number_fields == len(fields_under_test)

        # Can only check names and sizes for inserted gaps
        expected_sizes = [
            2,
            2,
            2,
            1,
            3,
            6,
        ]
        actual_sizes = [x._element["size"] for x in fields_under_test]
        assert expected_sizes == actual_sizes

        expected_names = [
            "f1",
            "",
            "f2",
            "",
            "f3",
            "",
        ]
        actual_names = [x._element["name"] for x in fields_under_test]
        assert expected_names == actual_names
