#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap import RegisterMap
from registermap.export.base.output import OutputBase


class MockOptions:
    def __init__(self):
        self.output = None
        self.packAlignment = None


class MockOutput(OutputBase):
    def __init__(self, output_options, licenseTextLines=list()):
        super().__init__(output_options, licenseTextLines=licenseTextLines)

    def generate(self, register_map, register_map_name):
        pass


class TestOutputBase:
    @pytest.fixture()
    def elements_context(self):
        self.file_path = "some/path"
        self.register_map = RegisterMap()

        self.register_map.addModule("m1")
        self.register_map.addModule("m2")

    def test_output_directory_property(self, elements_context, mocker):
        mock_options = MockOptions()
        mock_options.output = self.file_path

        mocker.patch(
            "registermap.export.base.output.OutputBase._OutputBase__validateOutputDirectory"
        )
        output_under_test = MockOutput(mock_options)

        assert self.file_path == output_under_test.outputDirectory

    def test_pack_alignment_option(self, elements_context, mocker):
        mock_options = MockOptions()
        mock_options.packAlignment = 10

        mocker.patch(
            "registermap.export.base.output.OutputBase._OutputBase__validateOutputDirectory"
        )
        output_under_test = MockOutput(mock_options)

        assert (
            mock_options.packAlignment
            == output_under_test.outputOptions.packAlignment
        )
