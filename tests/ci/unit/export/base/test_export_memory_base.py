#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import unittest.mock

import pytest

from registermap.export.base.memory import MemoryBase
from registermap.structure.memory.configuration import MemoryConfiguration


class MockMemory(MemoryBase):
    def __init__(self, memory_element, memory_size):
        super().__init__(memory_element, memory_size)

        self.mock_size_type = "std::uint_least8_t"

    @property
    def sizeType(self):
        return self.mock_size_type


class ElementsContext:
    def __init__(self):
        self.memory_space = MemoryConfiguration()
        self.memory_size = 10

        self.memory_under_test = MockMemory(self.memory_space, self.memory_size)


@pytest.fixture()
def elements_context() -> ElementsContext:
    return ElementsContext()


class TestMemory:
    def test_base_address_property(self, elements_context):
        assert (
            hex(elements_context.memory_space.baseAddress)
            == elements_context.memory_under_test.baseAddress
        )

    def test_memory_unit_size_bits_property(self, elements_context):
        assert (
            elements_context.memory_space.memoryUnitBits
            == elements_context.memory_under_test.memoryUnitBits
        )

    def test_size_property(self, elements_context):
        assert (
            elements_context.memory_size
            == elements_context.memory_under_test.size
        )
