#
# Copyright 2018 Russell Smiley
#
# This file is part of registermap.
#
# registermap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registermap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

import jinja2
import pytest

import registermap.export.base.template
from registermap import RegisterMap as ImportedRegisterMap
from registermap.export.c.elements.registerMap import RegisterMap
from registermap.export.c.output.module import ModuleTemplates
from tests.ci.unit.export.common_cpp_c.output import do_template_test

CREATE_DIRECTORY_MODULE_PATH = (
    "registermap.export.c.output.module.ModuleTemplates.createDirectory"
)
OPEN_MODULE_PATH = "registermap.export.commonCppC.output.module.open"


class TestCModuleTemplates:
    @pytest.fixture()
    def elements_context(self):
        self.expected_name = "someName"
        self.include_directory = "include/path"
        self.source_directory = "source/path"

        self.__setup_register_map()

    def __setup_register_map(self):
        registermap = ImportedRegisterMap()

        registermap.memory.baseAddress = 0x20F0

        self.m1 = registermap.addModule("m1")

        r1 = self.m1.addRegister("r1")

        r1.addField("f1", (0, 2))
        r1.addField("f2", (3, 4))
        r1.addField("f3", (5, 15))
        self.registermap = RegisterMap(self.expected_name, registermap)

    def testInitialCreatedFiles(self, elements_context, mocker):
        paths = ModuleTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.sourceDirectory = self.include_directory

        output_under_test = ModuleTemplates(paths, self.registermap)
        mock_template = mocker.create_autospec(jinja2.Template)
        mock_create_directory = mocker.patch(
            CREATE_DIRECTORY_MODULE_PATH, return_value=True
        )
        mocker.patch(OPEN_MODULE_PATH)
        mocker.patch.object(
            registermap.export.base.template.jinja2.Environment,
            "get_template",
            return_value=mock_template,
        )
        output_under_test.apply()

        expected_files = ["include/path/modules/m1.h"]

        assert expected_files == output_under_test.createdFiles

        mock_template.render.assert_called_once()

        mock_create_directory.assert_has_calls(
            [
                mocker.call(output_under_test.moduleDirectory),
            ]
        )

    def test_contiguous_fields_register_data(self, elements_context):
        expected_text = """/*
 *
 * Module: m1
 *
 *
 */

#ifndef SOMENAME_M1_H
#define SOMENAME_M1_H

#include <stdint.h>

#include "this/prefix/macro/extern.h"
#include "this/prefix/memory/memory.h"
#include "this/prefix/registermap.h"


SOMENAME_OPEN_EXTERN_C

struct someName_m1_r1_t
{
  uint8_t volatile f1:3;
  uint8_t volatile f2:2;
  uint16_t volatile f3:11;
};


#pragma pack(  )

struct someName_m1_t
{
  struct someName_m1_r1_t volatile r1;
};

#pragma pack()


SOMENAME_CLOSE_EXTERN_C

#endif
"""
        paths = ModuleTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.includePrefix = "this/prefix"
        paths.sourceDirectory = self.include_directory

        output_under_test = ModuleTemplates(paths, self.registermap)
        actual_lines = do_template_test(
            output_under_test, OPEN_MODULE_PATH, CREATE_DIRECTORY_MODULE_PATH
        )

        actual_text = "{}".format("".join(actual_lines))
        assert expected_text == actual_text


class TestNoncontiguousRegisterOutput:
    @pytest.fixture()
    def elements_context(self):
        self.expected_name = "someName"
        self.include_directory = "include/path"
        self.source_directory = "source/path"

        registermap = ImportedRegisterMap()
        registermap.memory.baseAddress = 0x20F0

        self.m1 = registermap.addModule("m1")

        self.registermap = RegisterMap(self.expected_name, registermap)

    def test_noncontiguous_fields_single_byte_register_data(
        self, elements_context
    ):
        r1 = self.m1.addRegister("r1")

        r1.addField("f1", (0, 1))
        r1.addField("f2", (4, 5))
        r1.addField("f3", (7, 7))

        expected_text = """/*
 *
 * Module: m1
 *
 *
 */

#ifndef SOMENAME_M1_H
#define SOMENAME_M1_H

#include <stdint.h>

#include "this/prefix/macro/extern.h"
#include "this/prefix/memory/memory.h"
#include "this/prefix/registermap.h"


SOMENAME_OPEN_EXTERN_C

struct someName_m1_r1_t
{
  uint8_t volatile f1:2;
  uint8_t volatile :2;
  uint8_t volatile f2:2;
  uint8_t volatile :1;
  uint8_t volatile f3:1;
};


#pragma pack(  )

struct someName_m1_t
{
  struct someName_m1_r1_t volatile r1;
};

#pragma pack()


SOMENAME_CLOSE_EXTERN_C

#endif
"""
        paths = ModuleTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.includePrefix = "this/prefix"
        paths.sourceDirectory = self.include_directory

        output_under_test = ModuleTemplates(paths, self.registermap)
        actual_lines = do_template_test(
            output_under_test, OPEN_MODULE_PATH, CREATE_DIRECTORY_MODULE_PATH
        )

        assert expected_text.splitlines(keepends=True) == actual_lines

    def test_noncontiguous_fields_starts_register_data(self, elements_context):
        r1 = self.m1.addRegister("r1")

        r1.addField("f1", (2, 4))
        r1.addField("f2", (5, 5))
        r1.addField("f3", (7, 7))

        expected_text = """/*
 *
 * Module: m1
 *
 *
 */

#ifndef SOMENAME_M1_H
#define SOMENAME_M1_H

#include <stdint.h>

#include "this/prefix/macro/extern.h"
#include "this/prefix/memory/memory.h"
#include "this/prefix/registermap.h"


SOMENAME_OPEN_EXTERN_C

struct someName_m1_r1_t
{
  uint8_t volatile :2;
  uint8_t volatile f1:3;
  uint8_t volatile f2:1;
  uint8_t volatile :1;
  uint8_t volatile f3:1;
};


#pragma pack(  )

struct someName_m1_t
{
  struct someName_m1_r1_t volatile r1;
};

#pragma pack()


SOMENAME_CLOSE_EXTERN_C

#endif
"""
        paths = ModuleTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.includePrefix = "this/prefix"
        paths.sourceDirectory = self.include_directory

        output_under_test = ModuleTemplates(paths, self.registermap)
        actual_lines = do_template_test(
            output_under_test, OPEN_MODULE_PATH, CREATE_DIRECTORY_MODULE_PATH
        )
        actual_text = "".join(actual_lines)
        assert expected_text == actual_text

    def test_noncontiguous_fields_multi_byte_register_data(
        self, elements_context
    ):
        r1 = self.m1.addRegister("r1")

        r1.addField("f1", (0, 1))
        r1.addField("f2", (4, 5))
        r1.addField("f3", (7, 9))

        expected_text = """/*
 *
 * Module: m1
 *
 *
 */

#ifndef SOMENAME_M1_H
#define SOMENAME_M1_H

#include <stdint.h>

#include "this/prefix/macro/extern.h"
#include "this/prefix/memory/memory.h"
#include "this/prefix/registermap.h"


SOMENAME_OPEN_EXTERN_C

struct someName_m1_r1_t
{
  uint8_t volatile f1:2;
  uint8_t volatile :2;
  uint8_t volatile f2:2;
  uint8_t volatile :1;
  uint8_t volatile f3:3;
  uint8_t volatile :6;
};


#pragma pack(  )

struct someName_m1_t
{
  struct someName_m1_r1_t volatile r1;
};

#pragma pack()


SOMENAME_CLOSE_EXTERN_C

#endif
"""
        paths = ModuleTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.includePrefix = "this/prefix"
        paths.sourceDirectory = self.include_directory

        output_under_test = ModuleTemplates(paths, self.registermap)
        actual_lines = do_template_test(
            output_under_test, OPEN_MODULE_PATH, CREATE_DIRECTORY_MODULE_PATH
        )

        assert expected_text.splitlines(keepends=True) == actual_lines


class TestEmptyModuleOutput:
    @pytest.fixture()
    def elements_context(self):
        self.expectedName = "someName"
        self.includeDirectory = "include/path"
        self.sourceDirectory = "source/path"

        registermap = ImportedRegisterMap()
        registermap.memory.baseAddress = 0x20F0

        m1 = registermap.addModule("m1")

        self.moduleUnderTest = m1

        self.registermap = RegisterMap(self.expectedName, registermap)

    def test_no_registers_empty_file(self, elements_context):
        expected_text = """/*
 *
 * Module: m1
 *
 *
 */

#ifndef SOMENAME_M1_H
#define SOMENAME_M1_H

#include <stdint.h>

#include "this/prefix/macro/extern.h"
#include "this/prefix/memory/memory.h"
#include "this/prefix/registermap.h"


// someName_m1 is an empty module

#endif
"""
        paths = ModuleTemplates.Paths()
        paths.includeDirectory = self.includeDirectory
        paths.includePrefix = "this/prefix"
        paths.sourceDirectory = self.includeDirectory

        output_under_test = ModuleTemplates(paths, self.registermap)
        actual_lines = do_template_test(
            output_under_test, OPEN_MODULE_PATH, CREATE_DIRECTORY_MODULE_PATH
        )

        assert expected_text.splitlines(keepends=True) == actual_lines

    def test_license_lines(self, elements_context):
        expected_text = """/*
 *
 * Module: m1
 *
 * This is a license text.
 * There could be some copyright applied.
 * Or any other distribution limitations.
 *
 */

#ifndef SOMENAME_M1_H
#define SOMENAME_M1_H

#include <stdint.h>

#include "this/prefix/macro/extern.h"
#include "this/prefix/memory/memory.h"
#include "this/prefix/registermap.h"


// someName_m1 is an empty module

#endif
"""

        input_license = [
            "This is a license text.",
            "There could be some copyright applied.",
            "Or any other distribution limitations.",
        ]

        paths = ModuleTemplates.Paths()
        paths.includeDirectory = self.includeDirectory
        paths.includePrefix = "this/prefix"
        paths.sourceDirectory = self.includeDirectory

        output_under_test = ModuleTemplates(
            paths, self.registermap, licenseTextLines=input_license
        )
        actual_lines = do_template_test(
            output_under_test, OPEN_MODULE_PATH, CREATE_DIRECTORY_MODULE_PATH
        )

        assert expected_text.splitlines(keepends=True) == actual_lines
