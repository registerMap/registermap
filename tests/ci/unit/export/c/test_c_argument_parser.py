#
# Copyright 2018 Russell Smiley
#
# This file is part of registermap.
#
# registermap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registermap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

import argparse

import pytest

from registermap.export.c.arguments import CArgumentParser


class TestCppArgumentParser:
    @pytest.fixture()
    def elements_context(self):
        self.argument_parser = argparse.ArgumentParser()

        self.language_parsers = self.argument_parser.add_subparsers(
            dest="language", title="language"
        )
        self.parser_under_test = CArgumentParser(self.language_parsers)

    def test_output_specified(self, elements_context):
        input_value = [
            "c",
            "some/path",
        ]
        args = self.argument_parser.parse_args(input_value)

        actual_value = self.parser_under_test.acquireOptions(args)

        assert input_value[1] == actual_value.output

    def test_missing_output_raises(self, elements_context):
        input_value = [
            "c",
        ]
        with pytest.raises(SystemExit):
            self.argument_parser.parse_args(input_value)

    def test_pack(self, elements_context):
        input_value = ["c", "some/path", "--pack", "2"]
        args = self.argument_parser.parse_args(input_value)

        actual_value = self.parser_under_test.acquireOptions(args)

        assert int(input_value[3]) == actual_value.packAlignment

    def test_default_include_path(self, elements_context):
        input_value = [
            "c",
            "some/path",
        ]
        args = self.argument_parser.parse_args(input_value)

        actual_value = self.parser_under_test.acquireOptions(args)

        assert "" == actual_value.includePrefix

    def test_include_path(self, elements_context):
        input_value = [
            "c",
            "some/path",
            "--include-prefix",
            "some/include/path",
        ]
        args = self.argument_parser.parse_args(input_value)

        actual_value = self.parser_under_test.acquireOptions(args)

        assert input_value[3] == actual_value.includePrefix
