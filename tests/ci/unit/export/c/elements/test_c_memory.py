#
# Copyright 2018 Russell Smiley
#
# This file is part of registermap.
#
# registermap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registermap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.export.c.elements.memory import Memory as CppMemory
from registermap.structure.memory.configuration import MemoryConfiguration


class TestCMemory:
    @pytest.fixture()
    def elements_context(self):
        self.memory_space = MemoryConfiguration()
        self.memory_size = 10

        self.memory_under_test = CppMemory(self.memory_space, self.memory_size)

    def test_default_alignment_property(self, elements_context):
        assert "" == self.memory_under_test.alignment

    def test_memory_size_type(self, elements_context):
        self.memory_space.memoryUnitBits = 32

        assert "uint32_t" == self.memory_under_test.sizeType
